/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _MARGUMENT_
#define _MARGUMENT_

#include "MParam.h"

enum class MNargs
{
  None = ' ',
  Star = '*',
  Plus = '+'  
};

class MArgument
{
  public:
    MArgument(void);
    MArgument(const MArgument& inArgument);
    MArgument(const MString& inName, const MSize& inNargs);
    MArgument(const MString& inName, const MNargs& inNargs);
    MArgument(const MString& inShort, const MString& inLong, const MSize& inNargs, bool inRequired = false);
    MArgument(const MString& inShort, const MString& inLong, const MNargs& inNargs, bool inRequired = false);
    ~MArgument(void);

    MArgument& Help(const MString& inHelp) { mHelp = inHelp; return *this; }
    MArgument& Prompt(const MString& inPrompt) { mPrompt = inPrompt; return *this; }
    MArgument& Name(const MString& inName) { mName = inName; return *this; }
    MArgument& Type(const MParamType& inType) { mType = inType; return *this; }
    MArgument& Min(const MFloat& inMin) { mMin = inMin; mHasMin = true; return *this; }
    MArgument& Max(const MFloat& inMax) { mMax = inMax; mHasMax = true; return *this; }
    MArgument& If(const MString& inArgName) { mContingent = inArgName; return *this; }
    MArgument& Optional(const MString& inPrompt) { mOptionalPrompt = inPrompt; return *this; }
    MArgument& Default(const MString& inValue) { mDefault = inValue; mHasDefault = true; return *this; }
    MArgument& Defer(void) { mDefers = true; return *this; }
    MArgument& Options(const MParamVector& inOptions) { mOptions = inOptions; return *this; }
    //TODO: MArgument& AddValidator(function pointer)

    MString GetArgumentName(void) const;
    MString GetDefault(void) const { return mDefault; }
    MParam GetParameter(void) const { return mParameter; }
    MString GetPrompt(void) const { return mPrompt; }
    MString GetOptionalPrompt(void) const { return mOptionalPrompt; }
    MParamVector GetOptions(void) const { return mOptions; }
    bool Defers(void) const { return mDefers; }

    bool operator==(const MArgument& inArgument);

  protected:

    bool HasLong(void) const { return !mLong.empty(); }
    bool HasShort(void) const { return !mShort.empty(); }

    MString GetLong(void) const { return mLong; }
    MString GetShort(void) const { return mShort; }

    void Read(int& ioIndex, const int& inCount, const char** argv, bool inExplicitFinal);
    void Validate(void);
    MString Usage(void);

    MString mShort;
    MString mLong;
    MString mName;
    MSize mNargs;
    MNargs mNargPattern;
    MString mHelp;
    MString mPrompt;
    MParamType mType;
    MFloat mMin;
    MFloat mMax;
    MString mOptionalPrompt;
    MString mContingent;
    MString mDefault;
    bool mRequired;
    bool mHasMin;
    bool mHasMax;
    bool mDefers;
    bool mHasDefault;

    MParam mParameter;
    MParamVector mOptions;

  friend class MArgParser;
};

#endif
