/*
 * Copyright (c) 2003 - 2020 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _MMESSAGE_
#define _MMESSAGE_

#include "MTypes.h"

// NOTE: Message Classes 0 through 999 are reserved for MPlant.  Define your own as 1000 or greater.

static const MUInteger kMsg_Null = 0;
static const MUInteger kMsg_Modified = 1;

class MMessage
{
  public:
    MMessage() { mClass = kMsg_Null; mDetail = kEmptyString; }
    MMessage(const MUInteger& inClass) { mClass = inClass; mDetail = kEmptyString; }
    MMessage(const MUInteger& inClass, const MString& inDetail) { mClass = inClass; mDetail = inDetail; }

    MUInteger GetClass() const { return mClass; }
    MString GetDetail() const { return mDetail; }


    void operator=(const MUInteger& inClass) { mClass = inClass; mDetail = kEmptyString; }
    bool operator==(const MUInteger& inClass) const { return mClass == inClass; }
    bool operator!=(const MUInteger& inClass) const { return mClass != inClass; }

  protected:
    MUInteger mClass;
    MString mDetail;
};

// Left-hand comparisons
bool operator==(const MUInteger& inClass, const MMessage& inMessage);
bool operator!=(const MUInteger& inClass, const MMessage& inMessage);

#endif
