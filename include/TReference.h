/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _TREFERENCE_
#define _TREFERENCE_

#include "MTypes.h"
#include <memory>
#include <typeinfo>

// This class wraps standard pointer types to check for null pointers
// and raise an exception instead of creating a segfault.

template <typename MElementType>
class TReference
{
  public:
    TReference() : mPointer() { Init(); }
    TReference(MElementType* inPtr) { mPointer.reset(inPtr); Init(); }
    TReference(const TReference& inPtr) { mPointer = inPtr.mPointer; mReadOnly = inPtr.mReadOnly; }
    TReference(const std::shared_ptr<MElementType>& inShared) { mPointer = inShared; Init(); }

    void SetReadOnly(void) { mReadOnly = true; }

    // Wrapped functions
    void Assign(MElementType* inPtr) { mPointer.reset(inPtr); }
    void Swap(MElementType* inPtr) { mPointer.swap(inPtr); }
    void Release(void) { mPointer.reset(); }
    MElementType* Get(void) { return mPointer.get(); }
    bool Defined(void) const { return mPointer.get() != nullptr; }
    bool IsNull(void) const { return mPointer.get() == nullptr; }

    // Operators
    MElementType& operator*() const { RaiseIfNull(); return *mPointer; }
    const std::shared_ptr<MElementType>& operator->() const { RaiseIfNull(); return mPointer; }
    void operator=(MElementType* inPtr) { RaiseIfReadOnly(); mPointer.reset(inPtr); }
    void operator=(const TReference& inPtr) { mPointer = inPtr.mPointer; }
    explicit operator bool() const { return Defined(); }

    // Conversion of TReference<Derived> class to TReference<Base> class object
    template <typename MBaseType>
    TReference<MBaseType> BasePointer(void) { return TReference<MBaseType>(mPointer); }

    template <typename MChildType>
    TReference<MChildType> DynamicPointer(void) { return TReference<MChildType>(std::dynamic_pointer_cast<MChildType>(mPointer)); }

  protected:

    void Init(void) { mReadOnly = false; }

    void RaiseIfNull(void) const
    {
      if (mPointer.get() == nullptr)
      {
        throw MEXCEPTION_BACKTRACE("Attempted to dereference a null pointer");
      }
    }

    void RaiseIfReadOnly(void) const
    {
      if (mReadOnly)
      {
        throw MEXCEPTION_BACKTRACE("Attempted to modify a read-only pointer");
      }
    }

    bool mReadOnly;
    std::shared_ptr<MElementType> mPointer;
};
        

#endif

