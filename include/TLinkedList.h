/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _TLINKEDLIST_
#define _TLINKEDLIST_

#include <cstddef>

// TNode class
template <typename T>
class TNode
{
  public:
    TNode(T& inData);
    TNode(const TNode&);
    ~TNode(void);
    void operator=(const TNode&);
    inline TNode* GetNext(void) { return mNext; }
    inline TNode* GetPrev(void) { return mPrev; }
    inline void SetNext(TNode* inNode) { mNext = inNode; }
    inline void SetPrev(TNode* inNode) { mPrev = inNode; }
    void GetItem(T& outItem);
    void SetItem(T& inItem);
    
    T& GetActualItem(void) { return mItem; }
  protected:
    T mItem;
    TNode* mNext;
    TNode* mPrev;  
};

template <typename T>
TNode<T>::TNode(T& inData)
{
  mItem = inData;
  mNext = NULL;
  mPrev = NULL;
}

template <typename T>
TNode<T>::TNode(const TNode& inNode)
{
  mItem = inNode.mItem;
  mNext = NULL;
  mPrev = NULL;
}

template <typename T>
TNode<T>::~TNode()
{
}

template <typename T>
void TNode<T>::operator=(const TNode& inNode)
{
  mItem = inNode.mItem;
}

template <typename T>
void TNode<T>::GetItem(T& outItem)
{
  outItem = mItem;
}

template <typename T>
void TNode<T>::SetItem(T& inItem)
{
  mItem = inItem;
}

// TLinkedList class
template <typename T>
class TLinkedList
{
  public:
    TLinkedList(void);
    TLinkedList(const TLinkedList&);
    ~TLinkedList(void);
    bool InsertBefore(int, T);
    void InsertAtEnd(T);
    void operator=(const TLinkedList&);
    int GetNumNodes(void);
    void Destruct(void);
    void MoveTargetToFront(void);
    bool GetItem(int, T&);
    void RemoveItem(int);
    T& operator[](int);
  protected:
    TNode<T>* mFirstNode;
    TNode<T>* mTargetNode;
    int mNumNodes;
};

template <typename T>
TLinkedList<T>::TLinkedList(void)
{
  mFirstNode = NULL;
  mTargetNode = NULL;
  mNumNodes = 0;
}

template <typename T>
TLinkedList<T>::TLinkedList(const TLinkedList& inList)
{
  *this = inList;
}

template <typename T>
TLinkedList<T>::~TLinkedList(void)
{
  Destruct();  
}

template <typename T>
void TLinkedList<T>::Destruct(void)
{
  TNode<T>* next;
  if (mFirstNode)
  {
    next = mFirstNode->GetNext();
    delete mFirstNode;
    while (next)
    {
      mFirstNode = next;
      next = next->GetNext();
      delete mFirstNode;
    }
  }
  mFirstNode = NULL;
  mTargetNode = NULL;
  mNumNodes = 0;  
}

template <typename T>
int TLinkedList<T>::GetNumNodes(void)
{
  return mNumNodes;
}

template <typename T>
void TLinkedList<T>::operator=(const TLinkedList& inList)
{
  int numNodes = inList.GetNumNodes();

  Destruct();

  TNode<T>* prev = NULL;
  TNode<T>* node = NULL;

  for (int i = 0; i < numNodes; i++)
  {
    node = new TNode<T>(inList[i]);
    if (i == 0)
    {
      mFirstNode = node;
    }
    else
    {
      prev->SetNext(node);
      node->SetPrev(prev);
    }
    prev = node;
  }
}

template <typename T>
void TLinkedList<T>::MoveTargetToFront(void)
{
  mTargetNode = mFirstNode;
}

template <typename T>
bool TLinkedList<T>::InsertBefore(int inIndex, T inItem)
{
  bool retVal = true;
  int i;
  
  TNode<T>* next = NULL;
  if (inIndex == 0)
  {
    if (mFirstNode)
      next = mFirstNode;
    else
    {
      mFirstNode = new TNode<T>(inItem);
      mFirstNode->SetNext(NULL);
      mFirstNode->SetPrev(NULL);
      mNumNodes++;
      return true;
    }
    mFirstNode = new TNode<T>(inItem);
    mFirstNode->SetNext(next);
    mFirstNode->SetPrev(NULL);
    next->SetPrev(mFirstNode);
    mTargetNode = mFirstNode;
    mNumNodes++;
  }
  else if (inIndex >= mNumNodes)
    retVal = false;
  else
  {
    next = mFirstNode->GetNext();
    for (i = 1; next && i < inIndex; i++)
    {
      next = next->GetNext();
    }
    if (i == inIndex)
    {  
      TNode<T>* newNode = new TNode<T>(inItem);
      TNode<T>* prev = next->GetPrev();
      newNode->SetPrev(prev);
      prev->SetNext(newNode);
      newNode->SetNext(next);
      next->SetPrev(newNode);
      mTargetNode = newNode;
      mNumNodes++;
    }
    else
      retVal = false;  
  }  
  return retVal;  
}

template <typename T>
void TLinkedList<T>::InsertAtEnd(T inItem)
{
  TNode<T>* next = mFirstNode;
  TNode<T>* prev = NULL;
  TNode<T>* newNode = NULL;
  
  if (!mFirstNode)
  {
    mFirstNode = new TNode<T>(inItem);
    newNode = mFirstNode;
  }
  else if (mTargetNode && !mTargetNode->GetNext())
  {
    newNode = new TNode<T>(inItem);
    mTargetNode->SetNext(newNode);
    newNode->SetPrev(mTargetNode);
  }  
  else
  {
    while (next)
    {
      prev = next;
      next = next->GetNext();
    }
    newNode = new TNode<T>(inItem);
    prev->SetNext(newNode);
    newNode->SetPrev(prev);
  }
  mTargetNode = newNode;
  mNumNodes++;  
}

// 0-indexed
template <typename T>
bool TLinkedList<T>::GetItem(int inIndex, T& outItem)
{
  bool retVal = true;
  TNode<T>* node = mFirstNode;
  if (inIndex == -1 && mTargetNode)
  {
    mTargetNode->GetItem(outItem);
    mTargetNode = mTargetNode->GetNext();
  }
  else if (inIndex > -1 && inIndex < mNumNodes)
  {
    for (int i = 0; i < inIndex; i++)
    {
      node = node->GetNext();
    }
    node->GetItem(outItem);
    mTargetNode = node;
  }
  else
    retVal = false;
  return retVal;    
}

template <typename T>
T& TLinkedList<T>::operator[](int inIndex)
{
  TNode<T>* node = mFirstNode;
  for (int i = 0; i < inIndex; i++)
  {
    node = node->GetNext();
  }
    
  if (node)
    return node->GetActualItem();
  
  throw "Bad operator[] in TLinkedList";
}

template <typename T>
void TLinkedList<T>::RemoveItem(int inIndex)
{
  TNode<T>* prev;
  TNode<T>* next;
  TNode<T>* node;
  
  if (inIndex == -2)          // Remove the node before the
  {                // target node.
    if (mTargetNode)
    {
      next = mTargetNode;
      prev = mTargetNode->GetPrev();
      if (prev)
      {
        mTargetNode = prev;
        prev = mTargetNode->GetPrev();
      }
      else
        prev = NULL;
          
      if (prev)
        prev->SetNext(next);
      else
        mFirstNode = next;

      if (next)
        next->SetPrev(prev);  // Next must be defined.

      delete mTargetNode;      // Remove the node.
      mNumNodes--;        // Decriment the number of nodes.
      mTargetNode = next;      // Put target back where it was.
    }
    else if (mFirstNode)
    {
      RemoveItem(mNumNodes - 1);
    }  
  }
  
  // Otherwise, remove a zero-indexed node.
  else if (inIndex == 0 && mFirstNode)
  {
    next = mFirstNode->GetNext();
    delete mFirstNode;
    mFirstNode = next;
    if (next)
      next->SetPrev(NULL);
    mNumNodes--;
  }
  else if (mFirstNode && inIndex > 0 && inIndex < mNumNodes)
  {
    node = mFirstNode;
    for (int i = 0; i < inIndex; i++)
    {
      node = node->GetNext();
    }
    prev = node->GetPrev();
    next = node->GetNext();
    
    if (prev)
      prev->SetNext(next);
    if (next)
      next->SetPrev(prev);  
    
    delete node;
    mNumNodes--;
  }  
}

#endif

