/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _UCONSTANTS_
#define _UCONSTANTS_

#include "MParam.h"

class UConstants
{
  public:
    static void Set(const MString& inKey, const MParam& inValue);
    static MParam Get(const MString& inKey);
    static MParam Get(const char* inStr) { return Get((MString)(inStr)); }
  protected:
    static MParamMap sConstantsMap;
};

#endif
