/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _MATTACHMENT_
#define _MATTACHMENT_

#include "MMessage.h"


const bool kStateOff = false;
const bool kStateOn = true;

class MAttachment
{
  public:
    MAttachment() { mMessageClass = kMsg_Null; mData = NULL; mOwned = true; mToggles = false; mEnabled = true; }
    MAttachment(const MUInteger& inMessageClass, void* inData = NULL, bool owned = true, bool inToggles = false, bool inEnabled = true) { mMessageClass = inMessageClass; mData = inData; mOwned = owned; mToggles = inToggles; mEnabled = inEnabled; }
    virtual ~MAttachment() {}
    
    MUInteger GetMessageClass(void) { return mMessageClass; }
    virtual void Execute(bool& outDoAction, const MMessage& inMessage, void* ioParam) = 0;
    bool Owned(void) { return mOwned; } 
    bool TogglesOnOff(void) { return mToggles; }
    void Enable(void) { mEnabled = true; }
    void Disable(void) { mEnabled = false; }
  protected:
    MUInteger mMessageClass;
    void* mData;
    bool mOwned;
    bool mToggles;
    bool mEnabled;
};

#endif
