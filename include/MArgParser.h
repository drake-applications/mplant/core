/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _MARGPARSER_
#define _MARGPARSER_

#include "MArgument.h"
#include "MArgPrompter.h"
#include "TReference.h"

class MArgParser
{
  public:
    MArgParser(void);
    MArgParser(const MString& inAppName);
    MArgParser(const MString& inAppName, const MString& inCommand);
    MArgParser(const MString& inAppName, const MString& inCommand, const MString& inDescription);
    ~MArgParser(void);

    MArgument& AddArgument(const MString& inShort, const MString& inLong, const MSize& inNargs = 0, bool inRequired = false);
    MArgument& AddArgument(const MString& inShort, const MString& inLong, const MNargs& inNargs, bool inRequired = false);
    MArgument& AddArgument(const MString& inLong, const MSize& inNargs = 0, bool inRequired = false);
    MArgument& AddArgument(const MString& inLong, const MNargs& inNargs, bool inRequired = false);

    MArgument& AddPositional(const MString& inName, const MSize& inNargs = 1);
    MArgument& AddFinal(const MString& inName);

    void Parse(int argc, const char** argv);
    bool Exists(const MString& inName);
    MParam GetParameter(const MString& inName);
    void SetParameterValue(const MString& inName, const MParam& inParam, bool inValidate = true);

    void SetPromptEnabled(bool inEnabled) { mPromptEnabled = inEnabled; }
    void PromptUser(void);
    void GetParameters(MParamMap& outParams);

    void Usage(void);

    void SetArgumentPrompter(MArgPrompter* inPrompter = kNull);
    void SetArgumentPrompter(const TReference<MArgPrompter>& inSharedPrompter);

  protected:
    void Initialize(void);
    MArgument& AddArgument(const MArgument& inArgument);
    MArgument& AddDeferrer(void);

    bool mHasFinal;
    bool mPromptEnabled;
    MString mApplication;
    MString mCommand;
    MString mDescription;
    MVector<MArgument> mArguments;
    MVector<MIndex> mPositionals;
    MVector<MIndex> mUsedArguments;
    MVector<MIndex> mUnusedArguments;
    MMap<MString, MIndex> mLongIndex;
    MMap<MString, MIndex> mShortIndex;
    MMap<MString, MIndex> mNamedIndex;
    MMap<MString, MIndex> mFullIndex;

    TReference<MArgPrompter> mPrompter;
};

#endif
