/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _MTYPES_
#define _MTYPES_

#include <cstdint>
#include <cstddef>
#include <string>
#include <vector>
#include <set>
#include <map>

// TYPES AND ABBREVIATIONS

using MInteger = int64_t;
using MUInteger = uint64_t;
using MFloat = double;
using MIndex = size_t;
using MSize = size_t;

#define MString std::string
#define MVector std::vector
#define MMap std::map
#define MSet std::set

static const MIndex kNotFound = SIZE_MAX;
static const MString kEmptyString = "";

#define kNull nullptr

// STRUCTURES

struct MColorRGB
{
  uint8_t r;
  uint8_t g;
  uint8_t b;
};

// MACROS

#define M_MIN(A, B) ((A > B) ? (B) : (A))
#define M_MAX(A, B) ((A > B) ? (A) : (B))


#define MMEMBER_READ_ONLY(TYPE, MEMBERNAME) \
        public: \
          TYPE Get##MEMBERNAME() const { return m##MEMBERNAME; } \
        protected: \
          TYPE m##MEMBERNAME;

#define MMEMBER_READ_WRITE(TYPE, MEMBERNAME) \
        public: \
          TYPE Get##MEMBERNAME() const { return m##MEMBERNAME; } \
          void Set##MEMBERNAME(const TYPE& value) { m##MEMBERNAME = value; } \
        protected: \
          TYPE m##MEMBERNAME;

#define MBOOLEAN_READ_ONLY(MEMBERNAME) \
        public: \
          bool Is##MEMBERNAME() const { return m##MEMBERNAME; } \
        protected: \
          bool m##MEMBERNAME;

#define MBOOLEAN_READ_WRITE(MEMBERNAME) \
        public: \
          bool Is##MEMBERNAME() const { return m##MEMBERNAME; } \
          void Set##MEMBERNAME(const bool& value) { m##MEMBERNAME = value; } \
        protected: \
          bool m##MEMBERNAME;

#define MEXCEPTION(A) (std::runtime_error(MString((A)) + MString(" in file ") + MString(__FILE__) + MString(" at line ") + std::to_string(__LINE__)))

MString MBACKTRACE(void);

#define MEXCEPTION_BACKTRACE(A) (std::runtime_error(MString((A)) + MString(" in file ") + MString(__FILE__) + MString(" at line ") + std::to_string(__LINE__) + MString("\n") + MBACKTRACE()))


#define MMAP_EXISTS(M, K) ((M).find((K)) != (M).end())
#define MMAP_VALUE(M, K) ((M).find((K))->second)

#endif
