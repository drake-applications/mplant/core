/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _USTRING_
#define _USTRING_

#include <algorithm>
#include <vector>
#include <list>

#include "MTypes.h"
#include "MParam.h"
#include "TArrayPointer.h"

using MStringVector = std::vector<MString>;

class UString
{
  public:
    static MStringVector Parse(const MString&, const char* inChars = " \n\t");
    static MStringVector Break(const MString&, const char* inChars = " \n\t");
    static MString Replace(MString, MString, MString, bool inEntireWord = false);
    static MString ReplaceOnce(MString, MString, MString, bool inEntireWord = false);
    static MSize Find(const MString& inString, const MString& inWhat, const MSize& inPosition = 0, const MSize& inRange = 0);
    static MSize Find(const MString& inString, const char inWhat, const MSize& inStart = 0, const MSize& inRange = 0);
    static MSize ReverseFind(const MString& inString, const MString& inWhat, const MSize& inPosition = kNotFound);
    static MSize ReverseFind(const MString& inString, const char inWhat, const MSize& inStart = kNotFound, const MSize& inRange = 0);
    static bool StartsWith(const MString& inString, const MString& inWhat);
    static bool EndsWith(const MString& inString, const MString& inWhat);
    static MString RemoveFormatting(const MString&);
    static MString Trim(const MString& inString, const char& inTrimChar = ' ');
    static MString TrimLeft(const MString& inString, const char& inTrimChar = ' ');
    static MString TrimRight(const MString& inString, const char& inTrimChar = ' ');
    static void MakeUpper(MString&);
    static void MakeLower(MString&);
    static MString ToUpper(const MString&);
    static MString ToLower(const MString&);
    static bool CheckWord(const MString&, const MString&, MIndex index = 0);
    static bool IsNumber(char);

    static MString IntegerToString(const MInteger&);
    static MString FloatToString(const MFloat&);

    static MString Format(const MString& inFormat, const MParamVector& inParams);

    static inline bool In(const MString& inSubject, const MStringVector& inList)
    {
      return std::find(inList.begin(), inList.end(), inSubject) != inList.end();
    }
};


#endif
