/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _MPARAM_H_
#define _MPARAM_H_

#include "MTypes.h"
#include "MStringEscaper.h"
#include "MBroadcaster.h"
#include "TReference.h"

enum class MParamType
{
  Invalid = -1,
  Exists = 0,
  Integer = 1,
  Float,
  Decimal,        // Not yet implemented
  Boolean,
  String,
  Data,
  Pointer,
  Reference,
  IntegerArray = 101,
  FloatArray,
  DecimalArray,   // Not yet implemented
  StringArray
};

// Forward declarations
class MProperty;
class MParam;

// Aliases
using MParamMap = MMap<MString, MParam>;
using MParamVector = MVector<MParam>;

class MParam : public MBroadcaster
{
  public:
    MParam(void);
    MParam(const MParam& inParam);
    MParam(const bool& inBool);
    MParam(const char* inStr);
    MParam(const MInteger& inInt);
    MParam(const MFloat& inFloat);
    MParam(const MString& inString);
    MParam(const MVector<char>& inData);
    MParam(const MVector<MInteger>& inInts);
    MParam(const MVector<MFloat>& inFloats);
    MParam(const MVector<MString>& inStrings);
    MParam(void* inPointer);
    MParam(const TReference<MParam>& inReference);

    MParamType GetType(void) const { return mType; }

    bool IsModified(void) { return mModified; }
    void ResetModified(void) { mModified = false; }

    void SetExpectedType(const MParamType& inType);
    MInteger NumElements(void) const;

    // Accessors or Casts
    MString StringValue(MStringEscaper* inEscaper = kNull) const;
    MInteger IntegerValue(void) const;
    MFloat FloatValue(void) const;
    bool BooleanValue(void) const;

    MVector<MInteger> IntegerArrayValue(void) const;
    MVector<MFloat> FloatArrayValue(void) const;
    MVector<MString> StringArrayValue(void) const;

    void* PointerValue(void) const;

    bool CastsToBoolean(void) const;
    bool CastsToInteger(void) const;
    bool CastsToFloat(void) const;

    // Conversions
    void ToBoolean(void);
    void ToInteger(void);
    void ToFloat(void);
    void ToString(void);

    void Invalidate(void);
    bool IsValid(void);

    MString Evaluate(MStringEscaper* inEscaper = kNull) const;

    // Assignments
    MParam& operator=(const MParam& inParam);
    MParam& operator=(const MInteger& inInt);
    MParam& operator=(const bool& inBool);
    MParam& operator=(const MFloat& inFloat);
    MParam& operator=(const char* inString);
    MParam& operator=(const MString& inString);
    MParam& operator=(const MVector<char>& inData);
    MParam& operator=(const MVector<MInteger>& inInts);
    MParam& operator=(const MVector<MFloat>& inFloat);
    MParam& operator=(const MVector<MString>& inStrings);
    MParam& operator=(const MProperty& inProperty);
    MParam& operator=(void* inPointer);
    MParam& operator=(const TReference<MParam>& inReference);

    // Casts
    operator MString() const { return StringValue(); }
    operator MInteger() const { return IntegerValue(); }
    operator MFloat() const { return FloatValue(); }
    operator bool() const { return BooleanValue(); }
    operator MVector<MString>() const { return StringArrayValue(); }
    operator MVector<MInteger>() const { return IntegerArrayValue(); }
    operator MVector<MFloat>() const { return FloatArrayValue(); }

    // Comparisons
    bool operator<(const MFloat& inValue) const;
    bool operator<=(const MFloat& inValue) const;
    bool operator>(const MFloat& inValue) const;
    bool operator>=(const MFloat& inValue) const;
    bool operator==(const MFloat& inValue) const;
    bool operator!=(const MFloat& inValue) const;

    bool operator==(const MString& inValue) const;
    bool operator!=(const MString& inValue) const;

    bool In(const MParamVector& inValues) const;

  protected:
    void Copy(const MParam& inParam);
    void Floats(MVector<MFloat>& outFloats) const; // Used for comparisons with float

    MParamType mType;
    MParamType mExpectedType;

    MInteger mIntegerValue;
    MFloat mFloatValue;
    MString mStringValue;
    MVector<char> mDataValue;
    MVector<MInteger> mIntegersValue;
    MVector<MFloat> mFloatsValue;
    MVector<MString> mStringsValue;
    void* mPointerValue;
    TReference<MParam> mReferenceValue;

    void Modified(void);
    bool mModified;
};

// Comparisons
bool operator<(const MFloat& inValue, const MParam& inParam);
bool operator<=(const MFloat& inValue, const MParam& inParam);
bool operator>(const MFloat& inValue, const MParam& inParam);
bool operator>=(const MFloat& inValue, const MParam& inParam);
bool operator==(const MFloat& inValue, const MParam& inParam);
bool operator!=(const MFloat& inValue, const MParam& inParam);

#endif
