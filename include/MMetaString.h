/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Historical Note
 *
 * The original MetaString parser on which this class is based was
 * written in Objective-C while I was at Paladin Logic.  It was used in
 * several iOS Apps.  I later ported it to VisualBasic for another
 * Paladin project.  I received express permission to port the parser to
 * C++ and again to Java, crediting Paladin Logic as a contributor.  I
 * have improved the parser with each port.
 *
 * For more information about Paladin Logic, see www.paladinlogic.com.
 */

#ifndef _MMETASTRING_H_
#define _MMETASTRING_H_

#include "MLexicon.h"
#include "MParam.h"
#include "MTypes.h"
#include "TPointer.h"

class MMetamorphicBlock;
class MMetamorphicTag;

using MMetamorphicTagVector = MVector<MMetamorphicTag*>;
using MMetamorphicBlockVector = MVector<MMetamorphicBlock*>;

class MMetaStringLexicon : public MLexicon
{
  public:
    MMetaStringLexicon(void);
    virtual ~MMetaStringLexicon(void) {}

    MMEMBER_READ_ONLY(MString, StartTag);
    MMEMBER_READ_ONLY(MString, EndTag);
    MMEMBER_READ_ONLY(MString, StartFunction);
    MMEMBER_READ_ONLY(MString, EndFunction);
    MMEMBER_READ_ONLY(MString, Quote);
    MMEMBER_READ_ONLY(MString, AltQuote);
    MMEMBER_READ_ONLY(MString, If);
    MMEMBER_READ_ONLY(MString, Else);
    MMEMBER_READ_ONLY(MString, ElseIf);
    MMEMBER_READ_ONLY(MString, End);
    MMEMBER_READ_ONLY(MString, Not);
    MMEMBER_READ_ONLY(MString, Exists);
    MMEMBER_READ_ONLY(MString, Literal);
    MMEMBER_READ_ONLY(MString, Value);
};

class MMetamorphicBlock
{
  public:
    MMetamorphicBlock(void);
    MMetamorphicBlock(const MString& inBlock);
    ~MMetamorphicBlock(void);
    bool Evaluate(MString& outString, const MParamMap& inParameters, const MParamMap& inOverrideParams, const TPointer<MMetaStringLexicon>& inLexicon, MStringEscaper* inEscaper = kNull);
    void AddTag(MMetamorphicTag* inTag);
    bool IsEmpty(void);
  protected:
    MString mBlock;
    MMetamorphicTagVector mTags;
};

class MMetamorphicTag
{
  public:
    MMetamorphicTag(const MString& inName, bool isStart, bool isEnd, const MString& inParameter);
    ~MMetamorphicTag(void);
    bool Evaluate(MString& outString, const MParamMap& inParameters, const MParamMap& inOverrideParams, const TPointer<MMetaStringLexicon>& inLexicon, MStringEscaper* inEscaper = kNull);
    void AddBlock(MMetamorphicBlock* inBlock);

    MMEMBER_READ_ONLY(MString, Name);
    MMEMBER_READ_WRITE(MString, Parameter);
    MBOOLEAN_READ_ONLY(StartOfBlock);
    MBOOLEAN_READ_ONLY(EndOfBlock);
  protected:
    MMetamorphicBlockVector mBlocks;
};

class MMetaString
{
  public:
    MMetaString(MMetaStringLexicon* inLexicon = kNull);
    MMetaString(const char* inStr, MMetaStringLexicon* inLexicon = kNull);
    MMetaString(const MString& inString, MMetaStringLexicon* inLexicon = kNull);
    ~MMetaString();
    void SetString(const MString& inString);

    MString Evaluate(const MParamMap& inParameters, MStringEscaper* inEscaper = kNull) const;
    MString Evaluate(const MParamMap& inParameters, const MParamMap& inOverrideParams, MStringEscaper* inEscaper = kNull) const;

    void operator=(const char* inStr) { SetString(MString(inStr)); }
    void operator=(const MString& inStr) { SetString(inStr); }

  protected:
    void SetLexicon(MMetaStringLexicon* inLexicon);
    void ParseString(const MString& inString);
    MMetamorphicBlock* ReadNextBlock(const MString& inString, MIndex& ioIndex, const MSize& inRange, MMetamorphicBlock* inParentBlock);
    MMetamorphicTag* ReadNextTag(const MString& inString, MIndex& ioIndex, const MSize& inRange);
    MString ReadTagParameter(const MString& inTag);

    MMetamorphicBlockVector mBlocks;
    TPointer<MMetaStringLexicon> mLexicon;
};

#endif
