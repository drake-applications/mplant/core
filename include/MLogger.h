/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _MLOGGER_
#define _MLOGGER_

#include "MTypes.h"
#include "MConcurrent.h"
#include <ostream>

enum class MLogLevel
{
  Debug = 0,
  Trace = 1,
  Inform = 2,
  Warn = 3,
  Error = 4,
  Fatal = 5,
  Silent = 100,
  NotSet
};

struct MStreamSpec
{
  bool mOwnsStream;
  std::ostream* mStream;
  MString mFormat;
  MLogLevel mLogLevel;
};

class MLogger : public MConcurrent
{
  public:
    MLogger(MLogger* inParent = kNull);
    MLogger(const MString& inName, MLogger* inParent = kNull);
    virtual ~MLogger(void);

    static MLogger& RootLogger(void)
    {
      static MLogger sRootLogger;
      return sRootLogger;
    }

    void AddDestination(const MString& inFile, const MString& inFormat, const MLogLevel& inLevel = MLogLevel::NotSet);
    void AddDefaultDestination(void);
    void SetName(const MString& inName) { mName = inName; }
    void SetLogLevel(MLogLevel inLevel) { mLogLevel = inLevel; mMinLogLevel = M_MIN(mMinLogLevel, inLevel); }
    void Fatal(const MString& inComment);
    void Error(const MString& inComment);
    void Warn(const MString& inComment);
    void Inform(const MString& inComment);
    void Trace(const MString& inComment);
    void Debug(const MString& inComment);

    bool Debug(void);

    MLogLevel GetLogLevel(void) { return mLogLevel; }

    static MLogLevel ReadLogLevel(const MString& inLevelStr);

  protected:
    void SetParent(MLogger* inParent);
    void Inherit(void);

    bool ShouldLog(const MLogLevel& inLevel);
    void EnterLog(const MLogLevel& inLevel, const MString& inComment, const MString& inChild = kEmptyString);
    void WriteToLog(const MLogLevel& inLevel, const MString& inComment, const MString& inChild = kEmptyString);

    MVector<MStreamSpec> mDestinations;
    MLogLevel mLogLevel;
    MLogLevel mMinLogLevel;
    MString mName;
    MLogger* mParent;
};

#endif
