/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _TARRAYPOINTER_H_
#define _TARRAYPOINTER_H_

#include "MTypes.h"
#include <memory>
#include <typeinfo>

// This class wraps standard pointer types to check for null pointers
// and raise an exception instead of creating a segfault.

template <typename MElementType>
class TArrayPointer
{
  public:
    TArrayPointer() : mPointer() {}
    TArrayPointer(MElementType inPtr[]) { mPointer.reset(inPtr); }
    TArrayPointer(const TArrayPointer& inPtr) { mPointer = inPtr.mPointer; }

    void Assign(MElementType inPtr[]) { mPointer.reset(inPtr); }
    void Swap(MElementType inPtr[]) { mPointer.swap(inPtr); }
    void Release(void) { mPointer.release(); }
    MElementType* Get(void) { return mPointer.get(); }
    bool Defined(void) const { return mPointer.get() != nullptr; }
    bool IsNull(void) const { return mPointer.get() == nullptr; }

    MElementType& operator*() const { RaiseIfNull(); return *mPointer; }
    const std::unique_ptr<MElementType>& operator->() const { RaiseIfNull(); return mPointer; }
    MElementType& operator[](MUInteger inIndex) { RaiseIfNull(); return mPointer[inIndex]; }
    void operator=(MElementType inPtr[]) { mPointer.reset(inPtr); }
    void operator=(const TArrayPointer& inPtr) { mPointer = inPtr.mPointer; }
    explicit operator bool() const { return Defined(); }

  protected:

    void RaiseIfNull(void) const
    {
      if (mPointer.get() == nullptr)
      {
        throw MEXCEPTION_BACKTRACE("Attempted to dereference a null array pointer");
      }
    }

    std::unique_ptr<MElementType[]> mPointer;
};


#endif

