/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _MPROPERTY_H_
#define _MPROPERTY_H_

#include "MParam.h"
#include "MListener.h"

class MProperty : public MBroadcaster, public MListener
{
  public:
    MProperty(void);
    MProperty(const MString& inName);
    MProperty(const MString& inName, const MParam& inParam);

    MString GetName(void) { return mName; }
    void SetName(const MString& inName) { mName = inName; }

    bool IsModified(void) { return mModified; }
    void ResetModified(void) { mModified = false; }

    MParam GetParam(void) const { return mParam; }
    MParam* GetParamPtr(void) { return &mParam; }

    MProperty& operator=(const MParam& inParam);
    MProperty& operator=(const MInteger& inInt);
    MProperty& operator=(const bool& inBool);
    MProperty& operator=(const MFloat& inFloat);
    MProperty& operator=(const char* inString);
    MProperty& operator=(const MString& inString);
    MProperty& operator=(const MVector<MInteger>& inInts);
    MProperty& operator=(const MVector<MFloat>& inFloat);
    MProperty& operator=(const MVector<MString>& inStrings);

    operator MParam() const { return GetParam(); }

  protected:
    virtual void ListenToMessage(const MMessage& inMessage, void* ioParam);

    MParam mParam;
    MString mName;
    bool mModified;
};

#define MPROPERTY_(TYPE, MEMBER_NAME, PARAM_NAME) \
        public: \
           MParam Get##MEMBER_NAME() const { return m##MEMBER_NAME.GetParam(); } \
           void Set##MEMBER_NAME(const MParam& inParam) \
           { \
             m##MEMBER_NAME.GetParamPtr()->SetExpectedType(TYPE); \
             m##MEMBER_NAME.SetName(PARAM_NAME); \
             m##MEMBER_NAME = inParam; \
             mProperties[PARAM_NAME] = &m##MEMBER_NAME; \
           } \
        protected: \
           MProperty m##MEMBER_NAME;

#endif

