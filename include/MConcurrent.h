/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _MCONCURRENT_
#define _MCONCURRENT_

#include <chrono>
#include <condition_variable>
#include <mutex>

using MStdUniqueLock = std::unique_lock<std::timed_mutex>;

class MConcurrent
{
  public:

    MConcurrent() { mTimeout = 0; mAttempts = 1; }

    MConcurrent(const size_t inTimeout, const size_t inAttempts)
    {
      mTimeout = inTimeout;
      mAttempts = inAttempts;
    }

    MConcurrent(const MConcurrent& inConcurrent)
    {
      mTimeout = inConcurrent.mTimeout;
      mAttempts = inConcurrent.mAttempts;
    }

    virtual ~MConcurrent() {}

  protected:

    bool GetLock(MStdUniqueLock& inUniqueLock)
    {
      bool hasLock = false;
      bool indefinite = false;
      size_t attempt = 0;
      size_t timeout = 0;


      if (mTimeout == 0)
      {
        timeout = 10;
        indefinite = true;
      }
      else
      {
        timeout = mTimeout;
      }

      std::chrono::milliseconds stdTimeout(timeout);

      while (!hasLock && attempt < mAttempts)
      {
        hasLock = inUniqueLock.try_lock_for(stdTimeout);

        if (!indefinite)
        {
          attempt++;
        }
      }

      return hasLock;
    }

    void WaitForCondition(MStdUniqueLock& inLock, MUInteger inWaitMilliseconds)
    {
      WaitForCondition(inLock, mCondition, inWaitMilliseconds);
    }

    void WaitForCondition(MStdUniqueLock& inLock, std::condition_variable_any& inCondition, MUInteger inWaitMilliseconds)
    {
      if (inWaitMilliseconds > 0)
      {
        // Wait until condition is met or until timeout occurs
        std::chrono::milliseconds period(inWaitMilliseconds);
        inCondition.wait_for(inLock, period);
      }
      else
      {
        // Infinite wait until condition met
        inCondition.wait(inLock);
      }
    }

    void SetConditionMet()
    {
      SetConditionMet(mCondition);
    }

    void SetConditionMet(std::condition_variable_any& inCondition)
    {
      inCondition.notify_one();
    }

    size_t mTimeout;
    size_t mAttempts;
    std::timed_mutex mMutex;
    std::condition_variable_any mCondition;
};

#endif

