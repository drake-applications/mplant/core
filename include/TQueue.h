/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _TQUEUE_
#define _TQUEUE_

#include "MConcurrent.h"

#include <queue>
#include <thread>
#include <atomic>

// TQueue class
template <typename T>
class TQueue : public MConcurrent
{
  public:
    TQueue(const size_t inTimeout = 0, const size_t inAttempts = 1) : MConcurrent(inTimeout, inAttempts)
    {
      mBreak = false;
    }
    virtual ~TQueue(void) {}
    void Signal(void);
    bool Enqueue(const T& inObject);
    bool Dequeue(T& outObject, MUInteger inWaitMilliseconds = 0);
    T Dequeue(MUInteger inWaitMilliseconds = 0); // Pointer version
  protected:
    std::queue<T> mQueue;
    std::atomic<bool> mBreak;
};

template <typename T>
void TQueue<T>::Signal(void)
{
  mBreak = true;
  SetConditionMet();
}

template <typename T>
bool TQueue<T>::Enqueue(const T& inObject)
{
  bool success = false;

  MStdUniqueLock lock(mMutex, std::defer_lock);
  if (GetLock(lock))
  {
    mQueue.push(inObject);
    lock.unlock();
    SetConditionMet();
    success = true;
  }

  return success;
}

template <typename T>
bool TQueue<T>::Dequeue(T& outObject, MUInteger inWaitMilliseconds)
{
  bool gotItem = false;

  MStdUniqueLock lock(mMutex, std::defer_lock);
  if (GetLock(lock))
  {
    if (mQueue.empty() && !mBreak)
    {
      WaitForCondition(lock, inWaitMilliseconds);
    }

    if (!mQueue.empty() && !mBreak)
    {
      outObject = mQueue.front();
      mQueue.pop();
      gotItem = true;
    }
    mBreak = false;
  }

  return gotItem;
}

template <typename T>
T TQueue<T>::Dequeue(MUInteger inWaitMilliseconds)
{
  T outObject = nullptr;

  MStdUniqueLock lock(mMutex, std::defer_lock);
  if (GetLock(lock))
  {
    if (mQueue.empty() && !mBreak)
    {
      WaitForCondition(lock, inWaitMilliseconds);
    }

    if (!mQueue.empty() && !mBreak)
    {
      outObject = mQueue.front();
      mQueue.pop();
    }
    mBreak = false;
  }

  return outObject;
}

#endif

