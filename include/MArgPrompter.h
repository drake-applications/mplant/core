/*
 * Copyright (c) 2003 - 2020 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _MARGPROMPTER_
#define _MARGPROMPTER_

#include "MArgument.h"

#define MARG_DEFAULT_PROMPT_INDICATOR "---> "

enum class MArgPromptResult
{
  None,
  Input,
  Abort,
  Ignore
};

class MArgPrompter
{
  public:
    MArgPrompter(const MString& inIndicator = MARG_DEFAULT_PROMPT_INDICATOR);
    virtual ~MArgPrompter(void);
    virtual MArgPromptResult PromptForInput(const MArgument& inArg, MParam& outInput);
    virtual void ShowMessage(const MString& inMessage);
    virtual void SetPromptIndicator(const MString& inIndicator) { mPromptIndicator = inIndicator; }
  protected:
    MString mPromptIndicator;
};

#endif
