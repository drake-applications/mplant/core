/*
 * Copyright (c) 2003 - 2020 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "UEnvironment.h"
#include <cstdlib>
#include <pwd.h>
#include <unistd.h>

MString UEnvironment::GetHomeDirectory(void)
{
  MString home;

  // TODO: Make this cross-platform compatible
  struct passwd* pwd = getpwuid(getuid());
  if (pwd)
  {
    home = pwd->pw_dir;
  }
  if (home.empty())
  {
    throw "Failed to determine home directory.";
  }

  return home;
}

MString UEnvironment::GetVariable(const MString& inEnvVariable)
{
  char* value = getenv(inEnvVariable.c_str());
  return value ? (MString)value : kEmptyString;
}

MString UEnvironment::GetFilePathSeparator(void)
{
#if defined(__WIN32)
  return "\\";
#else
  return "/";
#endif
}

void UEnvironment::ConcatenatePath(MString& ioPath, const MString& inAddendum)
{
  if (!inAddendum.empty())
  {
    ioPath.append(UEnvironment::GetFilePathSeparator() + inAddendum);
  }
}

void UEnvironment::ConcatenatePathComponents(MString& ioPath, const MParamVector& inParts)
{
  for (auto iter = inParts.begin(); iter != inParts.end(); ++iter)
  {
    UEnvironment::ConcatenatePath(ioPath, (*iter).StringValue());
  }
}

