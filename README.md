# MPlant Core Library (ALPHA v0.1)

MPlant is a collection of C++ libraries inspired by the old Metrowerks PowerPlant framework for the classic MacOS.  Unlike PowerPlant, MPlant is intended to be platform-agnostic and builds on the standard template library, so it can be used for many applications.

## License
MPlant is Copyright (c) by Drake Applications and is developed by Michael Drake, Jr.  It is licensed under the Apache 2.0 license.  See the License file for more info.

To see more of what we do <a href="http://www.drakeapplications.com">visit our website</a>.

## C++ Compatibility
MPlant requires at least C++11, as selected in the Makefile.

## Principles
Development of MPlant is based on the following principles:

- <b>Backwards Compatibility</b>:  Our philosophy is that APIs should be stable. If an API implementation changes, its interface should remain the same; or at least existing calls to those interfaces should not break.  Once MPlant gets to a stable 1.0 release, we intend to maintain backwards compatibility, going forward, for the life of the project.
- <b>Readability</b>:  Unreadable code is generally unmaintainable.  MPlant makes some std::stuff easier to read by using its own aliases.  For example, MString is precisely <code>std::string</code> and you can also do things like <code>MMap\<MString, MString\></code>, which is precisely <code>std::map\<std::string, std::string\></code>.  If you don't like the "M" notation, you can use <code>std::string</code> in your own code when using MPlant; it's the same.

## What's Inside the Core Library?

The core library includes the following:

- MArgParser:  An ArgParse-esque argument parser capable of prompting the user for required arguments if they are not provided, storing input as MParam objects.
- MParam:  A variant type that accepts numbers, strings, and vectors thereof, with conversion functions where appropriate.
- MProperty:  Named parameters (buit on top of MParam) that broadcast a message to listeners when modified.
- MPointer: A wrapper around std::unique_ptr that catches attempts to dereference the null pointer
- MReference: A wrapper around std::shared_ptr that catches attempts to dereference the null pointer
- MString:  Just an alias for std::string. (We didn't reinvent the string!)
- MLogger:  A thread-safe logger (uses std::mutex).
- MMetaString:  A parameterized string that morphs based on parameters passed in.  It is very helpful in parameterizing SQL queries for reuse but can be used for other things as well.
- UString:  Some extra string utilities that operate on std::string objects.
- UDateTime:  Utility for getting the current date/time (used by MLogger).
- UConstants:  Utilites for constants contained in a std::map.
- Also included are a number of aliases for various STL constructs, for example, MStringVector (which is just <code>std::vector\<std::string\></code>) and some macros that just make things more concise.
