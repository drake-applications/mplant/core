/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MFile.h"
#include "TArrayPointer.h"

// --------------------------------------------------------------
//  Constructor
// --------------------------------------------------------------

MFile::MFile(const MString& inFileName)
{
  mFileName = inFileName;
  mIsOpen = false;
  mFile = NULL;
}

// --------------------------------------------------------------
//  Destructor
// --------------------------------------------------------------

MFile::~MFile()
{
  if (mIsOpen)
  {
    Close();
  }
}

// --------------------------------------------------------------
//  Open
// --------------------------------------------------------------
// Opens a file for the given access ("r", "w", "a", etc.)

bool MFile::Open(const MString& inAccess)
{
  if (!mIsOpen)
  {
    mFile = fopen(mFileName.c_str(), inAccess.c_str());
    if (mFile)
    {
      mIsOpen = true;
    }
  }
  return mIsOpen;
}

// --------------------------------------------------------------
//  Close
// --------------------------------------------------------------
// Closes the file.

void MFile::Close(void)
{
  if (mIsOpen)
  {
    fclose(mFile);
  }
}

// --------------------------------------------------------------
//  Read
// --------------------------------------------------------------
// Reads the file into a string object.  The length of the file
// is determined, a buffer of that size is allocated, and the
// file is read in all at once.

bool MFile::Read(MString& outData)
{
  unsigned long position;
  bool result = false;
  
  if (mIsOpen && mFile)
  {
    fseek(mFile, 0, SEEK_END);
    position = ftell(mFile);
    rewind(mFile);

    TArrayPointer<char> buffer;
    buffer = new char[position + 1];
    fread(buffer.Get(), position, 1, mFile);
    outData = buffer.Get();

    result = true;
  }
  
  return result;
}

