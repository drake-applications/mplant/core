/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "UString.h"

#include <cstdarg>
#include <cstring>
#include <sstream>

// --------------------------------------------------------------
//  Parse
// --------------------------------------------------------------
// Parses a string into a vector of strings.

MStringVector UString::Parse(const MString& inString, const char* inChars)
{
  MStringVector strings;
  MString word;
  MSize charLength = strlen(inChars);
  MSize stringLength = inString.length();
  MSize charIndex;
  MSize i;
  bool isParseChar;
  
  for (i = 0; i < stringLength; i++)
  {
    isParseChar = false;
    for (charIndex = 0; !isParseChar && charIndex < charLength; charIndex++)
    {
      if (inString[i] == inChars[charIndex])
      {
        isParseChar = true;
      }
    }
    
    if (isParseChar)
    {
      if (word != "")
      {
        strings.push_back(word);
        word = "";
      }
    }
    else
    {
      word = word + inString[i];
      if (i == stringLength - 1)
      {
        strings.push_back(word);
      }
    }
  }
  
  return strings;
}

// --------------------------------------------------------------
//  Break
// --------------------------------------------------------------
// Break a string into a vector of strings.  It differs from parse
// in that the incoming characters are not dropped from the string.

MStringVector UString::Break(const MString& inString, const char* inChars)
{
  MStringVector strings;
  MString word;
  MSize charLength = strlen(inChars);
  MSize stringLength = inString.length();
  MSize charIndex;
  MSize i;
  bool isParseChar;
  
  for (i = 0; i < stringLength; i++)
  {
    isParseChar = false;
    for (charIndex = 0; !isParseChar && charIndex < charLength; charIndex++)
    {
      if (inString[i] == inChars[charIndex])
      {
        isParseChar = true;
      }
    }
    
    word = word + inString[i];  // This is basically the only difference between break and parse.

    if (isParseChar)
    {
      if (word != "")
      {
        strings.push_back(word);
        word = "";
      }
    }
    else
    {
      if (i == stringLength - 1)
      {
        strings.push_back(word);
      }
    }
  }
  
  return strings;
}

// --------------------------------------------------------------
//  Replace
// --------------------------------------------------------------
// Finds all instances of string "inWhat" inside of "inString"
// and replaces those with instances of "inWith".

MString UString::Replace(MString inString, MString inWhat, MString inWith, bool inEntireWord)
{
  MSize position = 0;
  MSize oldPosition = 0;
  MSize whatLength = inWhat.length();
  MString newString;
  
  while (position != kNotFound)
  {
    oldPosition = position;
    position = UString::Find(inString, inWhat, oldPosition);
    
    if (position != kNotFound)
    {
      if ((inEntireWord && CheckWord(inString, inWhat, position)) || !inEntireWord)
      {
        newString = newString + inString.substr(oldPosition, position - oldPosition) + inWith;
        position += whatLength;
      }
      else if (inEntireWord)
      {
        position++;
        newString = newString + inString.substr(oldPosition, position - oldPosition);
      }
    }
    else
    {
      newString = newString + inString.substr(oldPosition);
    }
  }
  
  return newString;
}

// --------------------------------------------------------------
//  ReplaceOnce
// --------------------------------------------------------------
// Finds first instances of string "inWhat" inside of "inString"
// and replaces those with instances of "inWith".

MString UString::ReplaceOnce(MString inString, MString inWhat, MString inWith, bool inEntireWord)
{
  bool found = false;
  MSize position = 0;
  MSize oldPosition = 0;
  MSize whatLength = inWhat.length();
  MString newString;
  
  while (position != kNotFound)
  {
    oldPosition = position;
    
    if (found)
    {
      position = kNotFound;
    }
    else
    {
      position = UString::Find(inString, inWhat, oldPosition);
    }
    
    if (position != kNotFound)
    {
      if ((inEntireWord && CheckWord(inString, inWhat, position)) || !inEntireWord)
      {
        newString = newString + inString.substr(oldPosition, position - oldPosition) + inWith;
        position += whatLength;
        found = true;
      }
      else if (inEntireWord)
      {
        position++;
        newString = newString + inString.substr(oldPosition, position - oldPosition);
        found = true;
      }
    }
    else
    {
      newString = newString + inString.substr(oldPosition);
    }
  }
  
  return newString;
}

// --------------------------------------------------------------
//  Find
// --------------------------------------------------------------
// Finds the index of a string within another string.

MSize UString::Find(const MString& inString, const MString& inWhat, const MSize& inStart, const MSize& inRange)
{
  MIndex index = 0;
  MSize strLength = inString.length();

  if (inRange > 0)
  {
    strLength = M_MIN(strLength, inRange);
  }

  MString::size_type position = inString.find(inWhat, inStart);

  if (position == MString::npos || position > inStart + strLength)
  {
    index = kNotFound;
  }
  else
  {
    index = position;
  }
  
  return index;
}

// --------------------------------------------------------------
//  Find
// --------------------------------------------------------------
// Finds the first occurrence of a character within a string.

MSize UString::Find(const MString& inString, const char inWhat, const MSize& inStart, const MSize& inRange)
{
  MIndex index;
  MSize strLength = inString.length();

  if (inRange > 0)
  {
    strLength = M_MIN(strLength, inStart + inRange);
  }

  for (index = inStart; index < strLength; ++index)
  {
    if (inString[index] == inWhat)
    {
      return index;
    }
  }

  return kNotFound;
}

// --------------------------------------------------------------
//  ReverseFind
// --------------------------------------------------------------
// Finds the last occurrence of a string within another string.

MSize UString::ReverseFind(const MString& inString, const MString& inWhat, const MSize& inStart)
{
  MIndex start = inStart == kNotFound ? std::string::npos : inStart;
  MIndex index = inString.rfind(inWhat, start);
  if (index == std::string::npos)
  {
    index = kNotFound;
  }

  return index;
}

// --------------------------------------------------------------
//  ReverseFind
// --------------------------------------------------------------
// Finds the last occurrence of a character within a string.

MSize UString::ReverseFind(const MString& inString, const char inWhat, const MSize& inStart, const MSize& inRange)
{
  MSize count;
  MIndex index = kNotFound;
  MSize strLength = inString.length();
  MSize searchLength = strLength;
  MIndex start = M_MIN(inStart, strLength - 1);

  if (inRange > 0)
  {
    searchLength = M_MIN(start + 1, inRange);
  }

  for (index = start, count = 0; count < searchLength; --index, ++count)
  {
    if (inString[index] == inWhat)
    {
      return index;
    }
  }

  return kNotFound;
}

// --------------------------------------------------------------
//  StartsWith
// --------------------------------------------------------------
// True if inString starts with inWhat

bool UString::StartsWith(const MString& inString, const MString& inWhat)
{
  bool startsWith = false;
  MSize strLength = inString.length();
  MSize searchLength = inWhat.length();

  if (searchLength <= strLength)
  {
    startsWith = true;

    for (MSize i = 0; startsWith && i < searchLength; i++)
    {
      startsWith = (inString[i] == inWhat[i]);
    }
  }

  return startsWith;
}

// --------------------------------------------------------------
//  EndsWith
// --------------------------------------------------------------
// True if inString ends with inWhat

bool UString::EndsWith(const MString& inString, const MString& inWhat)
{
  bool endsWith = false;
  MSize strLength = inString.length();
  MSize searchLength = inWhat.length();

  if (searchLength == 0)
  {
    return true;
  }

  if (searchLength <= strLength)
  {
    endsWith = true;

    MIndex strIdx = strLength - 1;
    MIndex searchIdx = searchLength - 1;

    while (endsWith)
    { 
      endsWith = (inString[strIdx] == inWhat[searchIdx]);
      if (searchIdx == 0)
      {
        break;
      }
      --strIdx;
      --searchIdx;
    }
  }

  return endsWith;
}

// --------------------------------------------------------------
//  RemoveFormatting
// --------------------------------------------------------------
//

MString UString::RemoveFormatting(const MString& inString)
{
  MString outString;
  
  MSize oldPosition = 0;
  MSize position = 0;
  
  while (position > kNotFound)
  {
    position = UString::Find(inString, "{", position);
    if (position == kNotFound)
    {
      outString = outString + inString.substr(oldPosition);
    }
    else
    {
      outString = outString + inString.substr(oldPosition, position - oldPosition);
      position = UString::Find(inString, "}", position);
      if (position != kNotFound)
      {
        position++;
      }
    }
    oldPosition = position;
  }
  
  return outString;
}

// --------------------------------------------------------------
//  CheckWord
// --------------------------------------------------------------
// Verifies that the word or phrase is complete.  E.g., if "in"
// is inWord, this will ensure that "fin" does not match.

bool UString::CheckWord(const MString& inString, const MString& inWord, MIndex inPosition)
{
  bool frontGood = false;
  bool endGood = false;
  MSize endPosition = 0;
  char nextChar = '\0';
 
  // Test front of word.
  if (inPosition == 0 || (inPosition > 0 && (isspace(inString[inPosition - 1]) || inString[inPosition - 1] == '>')))
  {
    frontGood = true;
    
    endPosition = inPosition + inWord.length();

    if (endPosition == inString.length())
    {
      endGood = true;
    }
    else if (endPosition < inString.length())
    {
      nextChar = inString[endPosition];
      
      if (!isalpha(nextChar) && !IsNumber(nextChar) && nextChar != '-')
      {
        endGood = true;
      }
    }
  }
  
  return frontGood && endGood;
}

// --------------------------------------------------------------
//  Trim
// --------------------------------------------------------------
// Removes spaces from the front and back of a string.

MString UString::Trim(const MString& inString, const char& inTrimChar)
{
  MSize len = inString.length();
  MSize start;
  MSize end;
  
  for (start = 0; inString[start] == inTrimChar; start++);
  for (end = len - 1; inString[end] == inTrimChar; end--);
  
  return inString.substr(start, end + 1 - start);
}

// --------------------------------------------------------------
//  TrimLeft
// --------------------------------------------------------------
// Removes spaces from the front of a string.

MString UString::TrimLeft(const MString& inString, const char& inTrimChar)
{
  MSize len = inString.length();
  MSize start;
  MSize end = len;
  
  for (start = 0; inString[start] == inTrimChar; start++);
  
  return inString.substr(start, end - start);
}

// --------------------------------------------------------------
//  TrimRight
// --------------------------------------------------------------
// Removes spaces from the back of a string.

MString UString::TrimRight(const MString& inString, const char& inTrimChar)
{
  MSize len = inString.length();
  MSize start = 0;
  MSize end;
  
  for (end = len - 1; inString[end] == inTrimChar; end--);
  
  return inString.substr(start, end + 1 - start);
}

// --------------------------------------------------------------
//  MakeLower
// --------------------------------------------------------------
// Makes a string lower case.

void UString::MakeLower(MString& ioString)
{
  MSize length = ioString.length();
  
  for (MSize i = 0; i < length; i++)
  {
    ioString[i] = tolower(ioString[i]);
  }
}

// --------------------------------------------------------------
//  MakeUpper
// --------------------------------------------------------------
// Makes a string upper case.

void UString::MakeUpper(MString& ioString)
{
  MSize length = ioString.length();
  
  for (MSize i = 0; i < length; i++)
  {
    ioString[i] = toupper(ioString[i]);
  }
}

// --------------------------------------------------------------
//  ToLower
// --------------------------------------------------------------
// Non-destructively returns a string in lower case.

MString UString::ToLower(const MString& inString)
{
  MString copy = inString;
  UString::MakeLower(copy);
  return copy;
}

// --------------------------------------------------------------
//  ToUpper
// --------------------------------------------------------------
// Non-destructively returns a string in upper case.

MString UString::ToUpper(const MString& inString)
{
  MString copy = inString;
  UString::MakeUpper(copy);
  return copy;
}

// --------------------------------------------------------------
//  IsNumber
// --------------------------------------------------------------
// Checks if a character falls between 0 and 9

bool UString::IsNumber(char inChar)
{
  return (inChar >= '0' && inChar <= '9');
}

// --------------------------------------------------------------
//  IntegerToString
// --------------------------------------------------------------
// Write a float to a string

MString UString::IntegerToString(const MInteger& inInteger)
{
  std::ostringstream strs;
  strs << inInteger;
  return strs.str();
}

// --------------------------------------------------------------
//  FloatToString
// --------------------------------------------------------------
// Write a float to a string

MString UString::FloatToString(const MFloat& inFloat)
{
  std::ostringstream strs;
  strs << inFloat;
  return strs.str();
}

// --------------------------------------------------------------
//  Format
// --------------------------------------------------------------
// A printf or python style string formatter

MString UString::Format(const MString& inFormat, const MParamVector& inParams)
{
  MString fmt = UString::Replace(inFormat, "%s", "{}");
  fmt = UString::Replace(fmt, "%d", "{}");

  for (auto iter = inParams.begin(); iter != inParams.end(); ++iter)
  {
    fmt = UString::ReplaceOnce(fmt, "{}", (*iter).StringValue());
  }

  return fmt;
}

