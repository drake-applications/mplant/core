/*
 * Copyright (c) 2003 - 2020 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MArgPrompter.h"
#include <iostream>
#include <cstdlib>

// --------------------------------------------------------------
//  Constructors
// --------------------------------------------------------------

MArgPrompter::MArgPrompter(const MString& inIndicator)
{
  mPromptIndicator = inIndicator;
}

// --------------------------------------------------------------
//  Destructor
// --------------------------------------------------------------
// 

MArgPrompter::~MArgPrompter(void)
{
}

// --------------------------------------------------------------
//  PromptForInput
// --------------------------------------------------------------
// Obtain a value from the user for the given argument.

MArgPromptResult MArgPrompter::PromptForInput(const MArgument& inArg, MParam& outInput)
{
  // The default implementation uses standard input to get a value
  // from the user.

  MString answer;
  MString input;

  MArgPromptResult result = MArgPromptResult::Input;

  // If this is an optional prompted parameter, ask the user if a value is desired.
  MString optionalPrompt = inArg.GetOptionalPrompt();
  if (!optionalPrompt.empty())
  {
    char inputChar = 0;
    do
    {
      std::cerr << optionalPrompt << " (Y/N)" << std::endl;
      std::cerr << mPromptIndicator;
      std::getline(std::cin, input);
      inputChar = input[0];
    }
    while (inputChar != 'Y' && inputChar != 'y' && inputChar != 'N' && inputChar != 'n');

    if (inputChar == 'N' || inputChar == 'n')
    {
      result = MArgPromptResult::Ignore;
    }
  }

  if (result != MArgPromptResult::Ignore)
  {
    // Now prompt for the value ...
    std::cerr << inArg.GetPrompt() << std::endl;

    MParamVector options = inArg.GetOptions();
    if (!options.empty())
    {
      bool accepted = false;
      MInteger option;
      MInteger numOptions = options.size();

      do
      {
        for (MInteger index = 0; index < numOptions; ++index)
        {
          std::cerr << "[" << index + 1 << "] - " << (MString)options[index] << std::endl;
        }
        std::cerr << mPromptIndicator;
        std::getline(std::cin, input);
        option = atoi(input.c_str());

        if (option > 0 && option <= numOptions)
        {
          answer = (MString)options[option - 1];
          accepted = true;
        }
        else
        {
          std::cerr << "Enter a value from 1 to " << numOptions << " to select an option." << std::endl;
          std::cerr << inArg.GetPrompt() << std::endl;
        }
      }
      while (!accepted);
    }
    else
    {
      std::cerr << mPromptIndicator;
      std::getline(std::cin, answer);
    }

    outInput = answer;
  }

  // The standard input prompter does not have an abort option.  The
  // user can ctrl-c to abort.

  return result;
}

// --------------------------------------------------------------
//  ShowMessage
// --------------------------------------------------------------
// Display a message to the user, typically that input was not
// valid.

void MArgPrompter::ShowMessage(const MString& inMessage)
{
  std::cerr << inMessage << std::endl;
}

