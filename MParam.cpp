/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MParam.h"
#include "MProperty.h"
#include "UString.h"

// --------------------------------------------------------------
//  MParam(void)
// --------------------------------------------------------------
// Default constructor

MParam::MParam(void)
{
  mType = MParamType::Exists;
  mExpectedType = MParamType::Invalid;
  mModified = false;
}

// --------------------------------------------------------------
//  MParam(const MParam)
// --------------------------------------------------------------
// Copy constructor

MParam::MParam(const MParam& inParam)
{
  Copy(inParam);
}

// --------------------------------------------------------------
//  MParam(const MInteger)
// --------------------------------------------------------------
// Create an integer parameter

MParam::MParam(const MInteger& inInteger)
{
  mType = MParamType::Integer;
  mExpectedType = MParamType::Invalid;
  mIntegerValue = inInteger;
  mModified = false;
}

// --------------------------------------------------------------
//  MParam(const bool)
// --------------------------------------------------------------
// Create a boolean parameter

MParam::MParam(const bool& inBool)
{
  mType = MParamType::Boolean;
  mExpectedType = MParamType::Invalid;
  mIntegerValue = inBool ? 1 : 0;
  mModified = false;
}

// --------------------------------------------------------------
//  MParam(const MFloat)
// --------------------------------------------------------------
// Create a float parameter

MParam::MParam(const MFloat& inFloat)
{
  mType = MParamType::Float;
  mExpectedType = MParamType::Invalid;
  mFloatValue = inFloat;
  mModified = false;
}

// --------------------------------------------------------------
//  MParam(const char*)
// --------------------------------------------------------------
// Create a boolean parameter

MParam::MParam(const char* inStr)
{
  mType = MParamType::String;
  mExpectedType = MParamType::Invalid;
  mStringValue = inStr;
}

// --------------------------------------------------------------
//  MParam(const MString)
// --------------------------------------------------------------
// Create a string parameter

MParam::MParam(const MString& inString)
{
  mType = MParamType::String;
  mExpectedType = MParamType::Invalid;
  mStringValue = inString;
  mModified = false;
}

// --------------------------------------------------------------
//  MParam(const MVector<char>)
// --------------------------------------------------------------
// Create a data parameter (arbitrary binary data)

MParam::MParam(const MVector<char>& inData)
{
  mType = MParamType::Data;
  mExpectedType = MParamType::Invalid;
  mDataValue = inData;
  mModified = false;
}

// --------------------------------------------------------------
//  MParam(const MVector<MInteger>)
// --------------------------------------------------------------
// Create a parameter as a list of integers

MParam::MParam(const MVector<MInteger>& inIntegers)
{
  mType = MParamType::IntegerArray;
  mExpectedType = MParamType::Invalid;
  mIntegersValue = inIntegers;
  mModified = false;
}

// --------------------------------------------------------------
//  MParam(const MVector<MFloat>)
// --------------------------------------------------------------
// Create a parameter as a list of floats

MParam::MParam(const MVector<MFloat>& inFloats)
{
  mType = MParamType::FloatArray;
  mExpectedType = MParamType::Invalid;
  mFloatsValue = inFloats;
  mModified = false;
}

// --------------------------------------------------------------
//  MParam(const MVector<MString>)
// --------------------------------------------------------------
// Create a parameter as a list of strings

MParam::MParam(const MVector<MString>& inStrings)
{
  mType = MParamType::StringArray;
  mExpectedType = MParamType::Invalid;
  mStringsValue = inStrings;
  mModified = false;
}

// --------------------------------------------------------------
//  MParam(const MVector<MString>)
// --------------------------------------------------------------
// Create a parameter whose value is a void pointer

MParam::MParam(void* inPointer)
{
  mType = MParamType::Pointer;
  mExpectedType = MParamType::Invalid;
  mPointerValue = inPointer;
  mModified = false;
}

// --------------------------------------------------------------
//  MParam(const MVector<MString>)
// --------------------------------------------------------------
// Create a parameter whose value is a reference to another
// parameter

MParam::MParam(const TReference<MParam>& inReference)
{
  mType = MParamType::Reference;
  mExpectedType = MParamType::Invalid;
  mReferenceValue = inReference;
  mModified = false;
}

// --------------------------------------------------------------
//  SetExpectedType
// --------------------------------------------------------------
// Lock the type of this parameter so that assignments to other
// types are not allowed

void MParam::SetExpectedType(const MParamType& inType)
{
  mExpectedType = inType;
}

// --------------------------------------------------------------
//  Copy
// --------------------------------------------------------------
// Copy values from the incoming MParam

void MParam::Copy(const MParam& inParam)
{
  mType = inParam.mType;
  mExpectedType = inParam.mExpectedType;

  if (mType == MParamType::Integer || mType == MParamType::Boolean)
  {
    mModified = mIntegerValue != inParam.mIntegerValue;
    mIntegerValue = inParam.mIntegerValue;
  }
  else if (mType == MParamType::Float)
  {
    mModified = mFloatValue != inParam.mFloatValue;
    mFloatValue = inParam.mFloatValue;
  }
  else if (mType == MParamType::String)
  {
    mModified = mStringValue != inParam.mStringValue;
    mStringValue = inParam.mStringValue;
  }
  else if (mType == MParamType::Pointer)
  {
    mModified = true;
    mPointerValue = inParam.mPointerValue;
  }
  else if (mType == MParamType::Reference)
  {
    mModified = true;
    mReferenceValue = inParam.mReferenceValue;
  }
  else if (mType == MParamType::IntegerArray)
  {
    mModified = true;
    mIntegersValue = inParam.mIntegersValue;
  }
  else if (mType == MParamType::FloatArray)
  {
    mModified = true;
    mFloatsValue = inParam.mFloatsValue;
  }
  else if (mType == MParamType::StringArray)
  {
    mModified = true;
    mStringsValue = inParam.mStringsValue;
  }
}

// --------------------------------------------------------------
//  Assignment Operator (MParam)
// --------------------------------------------------------------
// Copy values from the incoming MParam

MParam& MParam::operator=(const MParam& inParam)
{
  Copy(inParam);
  Modified();
  return *this;
}

// --------------------------------------------------------------
//  Assignment Operator (MProperty)
// --------------------------------------------------------------
// Copy values from the incoming MProperty

MParam& MParam::operator=(const MProperty& inProperty)
{
  Copy(inProperty.GetParam());
  Modified();
  return *this;
}

// --------------------------------------------------------------
//  Assignment Operator (MInteger)
// --------------------------------------------------------------
// Assign an integer value to the parameter

MParam& MParam::operator=(const MInteger& inInt)
{
  if (mExpectedType != MParamType::Invalid && mType != MParamType::Integer)
  {
    throw MEXCEPTION_BACKTRACE("Attempted to assign integer to non-integer parameter.");
  }
  mType = MParamType::Integer;
  mModified = (mIntegerValue != inInt);
  mIntegerValue = inInt;
  Modified();
  return *this;
}

// --------------------------------------------------------------
//  Assignment Operator (bool)
// --------------------------------------------------------------
// Assign a boolean value to the parameter

MParam& MParam::operator=(const bool& inBool)
{
  if (mExpectedType != MParamType::Invalid && mType != MParamType::Boolean)
  {
    throw MEXCEPTION_BACKTRACE("Attempted to assign boolean to non-boolean parameter.");
  }
  mType = MParamType::Boolean;
  mModified = (mIntegerValue != (inBool ? 1 : 0));
  mIntegerValue = inBool ? 1 : 0;
  Modified();
  return *this;
}

// --------------------------------------------------------------
//  Assignment Operator (MFloat)
// --------------------------------------------------------------
// Assign a double floating point value to the parameter

MParam& MParam::operator=(const MFloat& inFloat)
{
  if (mExpectedType != MParamType::Invalid && mType != MParamType::Float)
  {
    throw MEXCEPTION_BACKTRACE("Attempted to assign float to non-float parameter.");
  }
  mType = MParamType::Float;
  mModified = (mFloatValue != inFloat);
  mFloatValue = inFloat;
  Modified();
  return *this;
}

// --------------------------------------------------------------
//  Assignment Operator (const char*)
// --------------------------------------------------------------
// Assign a string literal value to the parameter

MParam& MParam::operator=(const char* inStr)
{
  if (mExpectedType != MParamType::Invalid && mType != MParamType::String)
  {
    throw MEXCEPTION_BACKTRACE("Attempted to assign string to non-string parameter.");
  }
  mType = MParamType::String;
  mStringValue = inStr;
  Modified();
  return *this;
}

// --------------------------------------------------------------
//  Assignment Operator (MString)
// --------------------------------------------------------------
// Assign a string value to the parameter

MParam& MParam::operator=(const MString& inString)
{
  if (mExpectedType != MParamType::Invalid && mType != MParamType::String)
  {
    throw MEXCEPTION_BACKTRACE("Attempted to assign string to non-string parameter.");
  }
  mType = MParamType::String;
  mModified = (mStringValue != inString);
  mStringValue = inString;
  Modified();
  return *this;
}

// --------------------------------------------------------------
//  Assignment Operator (const MVector<char>)
// --------------------------------------------------------------
// Assign a data value to the parameter

MParam& MParam::operator=(const MVector<char>& inData)
{
  if (mExpectedType != MParamType::Invalid && mType != MParamType::Data)
  {
    throw MEXCEPTION_BACKTRACE("Attempted to assign data to non-data parameter.");
  }
  mType = MParamType::Data;
  mModified = (mDataValue != inData);
  mDataValue = inData;
  Modified();
  return *this;
}

// --------------------------------------------------------------
//  Assignment Operator (const MVector<MInteger>)
// --------------------------------------------------------------
// Assign a vector of integers to the parameter

MParam& MParam::operator=(const MVector<MInteger>& inInts)
{
  if (mExpectedType != MParamType::Invalid && mType != MParamType::IntegerArray)
  {
    throw MEXCEPTION_BACKTRACE("Attempted to assign integer array to non-integer-array parameter.");
  }
  mType = MParamType::IntegerArray;
  mIntegersValue = inInts;
  mModified = true;
  Modified();
  return *this;
}

// --------------------------------------------------------------
//  Assignment Operator (const MVector<MFloat>)
// --------------------------------------------------------------
// Assign a vector of floats to the parameter

MParam& MParam::operator=(const MVector<MFloat>& inFloats)
{
  if (mExpectedType != MParamType::Invalid && mType != MParamType::FloatArray)
  {
    throw MEXCEPTION_BACKTRACE("Attempted to assign float array to non-float-array parameter.");
  }
  mType = MParamType::FloatArray;
  mFloatsValue = inFloats;
  mModified = true;
  Modified();
  return *this;
}

// --------------------------------------------------------------
//  Assignment Operator (const MVector<MString>)
// --------------------------------------------------------------
// Assign a vector of strings to the parameter

MParam& MParam::operator=(const MVector<MString>& inStrings)
{
  if (mExpectedType != MParamType::Invalid && mType != MParamType::StringArray)
  {
    throw MEXCEPTION_BACKTRACE("Attempted to assign string array to non-string-array parameter.");
  }
  mType = MParamType::StringArray;
  mStringsValue = inStrings;
  mModified = true;
  Modified();
  return *this;
}

// --------------------------------------------------------------
//  Assignment Operator (void*)
// --------------------------------------------------------------
// Assign a void pointer value to the parameter

MParam& MParam::operator=(void* inPointer)
{
  if (mExpectedType != MParamType::Invalid && mType != MParamType::Pointer)
  {
    throw MEXCEPTION_BACKTRACE("Attempted to assign pointer to non-pointer parameter.");
  }
  mType = MParamType::Pointer;
  mPointerValue = inPointer;
  mModified = true;
  Modified();
  return *this;
}

// --------------------------------------------------------------
//  Assignment Operator (TReference<MParam>)
// --------------------------------------------------------------
// Assign a reference to a different parameter to this parameter

MParam& MParam::operator=(const TReference<MParam>& inReference)
{
  if (mExpectedType != MParamType::Invalid && mType != MParamType::Reference)
  {
    throw MEXCEPTION_BACKTRACE("Attempted to assign referece to non-reference parameter.");
  }
  mType = MParamType::Reference;
  mReferenceValue = inReference;
  mModified = true;
  Modified();
  return *this;
}

// Comparisons

// --------------------------------------------------------------
//  Floats
// --------------------------------------------------------------
// If the parameter can be converted to a vector of floats, write
// them to the supplied vector

void MParam::Floats(MVector<MFloat>& outFloats) const
{
  if (mType == MParamType::Integer || mType == MParamType::Boolean)
  {
    outFloats.push_back((MFloat)mIntegerValue);
  }
  else if (mType == MParamType::Float)
  {
    outFloats.push_back(mFloatValue);
  }
  else if (mType == MParamType::IntegerArray)
  {
    for (auto iter = mIntegersValue.begin(); iter != mIntegersValue.end(); ++iter)
    {
      outFloats.push_back((MFloat)(*iter));
    }
  }
  else if (mType == MParamType::FloatArray)
  {
    outFloats = mFloatsValue;
  }
  else
  {
    MEXCEPTION("Cannot compare this type of parameter to a floating point number.");
  }
}

// --------------------------------------------------------------
//  NumElements
// --------------------------------------------------------------
// Get the number of elements in the parameter

MInteger MParam::NumElements(void) const
{
  MInteger numElements = 1;
  if (mType == MParamType::StringArray)
  {
    numElements = mStringsValue.size();
  }
  else if (mType == MParamType::IntegerArray)
  {
    numElements = mIntegersValue.size();
  }
  else if (mType == MParamType::FloatArray)
  {
    numElements = mFloatsValue.size();
  }
  return numElements;
}
  
// --------------------------------------------------------------
//  Operator <
// --------------------------------------------------------------
// Are all values of the parameter less than the given value?

bool MParam::operator<(const MFloat& inValue) const
{
  bool compares = true;

  MVector<MFloat> floats;
  Floats(floats);

  for (auto iter = floats.begin(); compares && iter != floats.end(); ++iter)
  {
    compares = (*iter) < inValue;
  }

  return compares;
}

// --------------------------------------------------------------
//  Operator <=
// --------------------------------------------------------------
// Are all values of the parameter less than or equal to the
// given value?

bool MParam::operator<=(const MFloat& inValue) const
{
  bool compares = true;

  MVector<MFloat> floats;
  Floats(floats);

  for (auto iter = floats.begin(); compares && iter != floats.end(); ++iter)
  {
    compares = (*iter) <= inValue;
  }

  return compares;
}

// --------------------------------------------------------------
//  Operator >
// --------------------------------------------------------------
// Are all values of the parameter greater than the given value?

bool MParam::operator>(const MFloat& inValue) const
{
  bool compares = true;

  MVector<MFloat> floats;
  Floats(floats);

  for (auto iter = floats.begin(); compares && iter != floats.end(); ++iter)
  {
    compares = (*iter) > inValue;
  }

  return compares;
}

// --------------------------------------------------------------
//  Operator >=
// --------------------------------------------------------------
// Are all values of the parameter greater than or equal to the
// given value?

bool MParam::operator>=(const MFloat& inValue) const
{
  bool compares = true;

  MVector<MFloat> floats;
  Floats(floats);

  for (auto iter = floats.begin(); compares && iter != floats.end(); ++iter)
  {
    compares = (*iter) >= inValue;
  }

  return compares;
}

// --------------------------------------------------------------
//  Operator ==
// --------------------------------------------------------------
// Are all values of the parameter equal to the given value?

bool MParam::operator==(const MFloat& inValue) const
{
  bool compares = true;

  MVector<MFloat> floats;
  Floats(floats);

  for (auto iter = floats.begin(); compares && iter != floats.end(); ++iter)
  {
    compares = (*iter) == inValue;
  }

  return compares;
}

// --------------------------------------------------------------
//  Operator !=
// --------------------------------------------------------------
// Are all values of the parameter not equal to the given value?

bool MParam::operator!=(const MFloat& inValue) const
{
  bool compares = true;

  MVector<MFloat> floats;
  Floats(floats);

  for (auto iter = floats.begin(); compares && iter != floats.end(); ++iter)
  {
    compares = (*iter) != inValue;
  }

  return compares;
}

// --------------------------------------------------------------
//  Operator == (MString)
// --------------------------------------------------------------
// Is the string value of this param equal to the given string?

bool MParam::operator==(const MString& inValue) const
{
  return StringValue() == inValue;
}

// --------------------------------------------------------------
//  Operator == (MString)
// --------------------------------------------------------------
// Is the param's string value not equal to the given string?

bool MParam::operator!=(const MString& inValue) const
{
  return StringValue() != inValue;
}

// --------------------------------------------------------------
//  Operator == (MString)
// --------------------------------------------------------------
// Is the param's string value not equal to the given string?

bool MParam::In(const MParamVector& inValues) const
{
  bool found = false;

  for (auto iter = inValues.begin(); !found && iter != inValues.end(); ++iter)
  {
    if (StringValue() == (*iter).StringValue())
    {
      found = true;
    }
  }

  return found;
}

// --------------------------------------------------------------
//  Invalidate
// --------------------------------------------------------------
// Force the parameter to be invalid

void MParam::Invalidate(void)
{
  mType = MParamType::Invalid;
}

// --------------------------------------------------------------
//  IsValid
// --------------------------------------------------------------
// Is the parameter valid?  Does it have a type?

bool MParam::IsValid(void)
{
  return mType != MParamType::Invalid;
}

// --------------------------------------------------------------
//  StringValue()
// --------------------------------------------------------------
// Get the value of the parameter as a string

MString MParam::StringValue(MStringEscaper* inEscaper) const
{
  MString value;
  MString separator;
  bool isFirst = true;

  if (inEscaper)
  {
    separator = inEscaper->Separator();
  }
  else
  {
    separator = MStringEscaper::DefaultSeparator();
  }

  if (mType == MParamType::Invalid)
  {
    throw MEXCEPTION("Cannot cast Invalid parameter to String");
  }
  else if (mType == MParamType::Exists)
  {
    value = "";
  }
  else if (mType == MParamType::Boolean)
  {
    value = (mIntegerValue == 0 ? "false" : "true");
  }
  else if (mType == MParamType::Integer)
  {
    value = UString::IntegerToString(mIntegerValue);
  }
  else if (mType == MParamType::Float)
  {
    value = UString::FloatToString(mFloatValue);
  }
  else if (mType == MParamType::String)
  {
    value = mStringValue;
  }
  else if (mType == MParamType::Data)
  {
    value = reinterpret_cast<const char*>(&mDataValue[0]);
  }
  else if (mType == MParamType::IntegerArray)
  {
    if (mIntegersValue.size() > 0)
    {
      for (MVector<MInteger>::const_iterator it = mIntegersValue.begin(); it != mIntegersValue.end(); ++it)
      {
        if (isFirst)
        {
          value += UString::IntegerToString(*it);
          isFirst = false;
        }
        else
        {
          value += separator + UString::IntegerToString(*it);
        }
      }
    }
  }
  else if (mType == MParamType::FloatArray)
  {
    if (mFloatsValue.size() > 0)
    {
      for (MVector<MFloat>::const_iterator it = mFloatsValue.begin(); it != mFloatsValue.end(); it++)
      {
        if (isFirst)
        {
          value += UString::FloatToString(*it);
          isFirst = false;
        }
        else
        {
          value += separator + UString::FloatToString(*it);
        }
      }
    }
  }
  else if (mType == MParamType::StringArray)
  {
    if (mStringsValue.size() > 0)
    {
      for (MVector<MString>::const_iterator it = mStringsValue.begin(); it != mStringsValue.end(); it++)
      {
        if (isFirst)
        {
          value += *it;
          isFirst = false;
        }
        else
        {
          value += separator + *it;
        }
      }
    }
  }
  else if (mType == MParamType::Reference)
  {
    value = mReferenceValue->StringValue();
  }

  return value;
}

// --------------------------------------------------------------
//  IntegerValue
// --------------------------------------------------------------
// If possible, obtain the integer equivalent value of the param

MInteger MParam::IntegerValue(void) const
{
  MInteger value = 0;

  if (mType == MParamType::Invalid)
  {
    throw MEXCEPTION("Cannot cast an Invalid parameter to Integer");
  }
  else if (mType == MParamType::Exists)
  {
    value = 1;
  }
  else if (mType == MParamType::Integer || mType == MParamType::Boolean)
  {
    value = mIntegerValue;
  }
  else if (mType == MParamType::Float)
  {
    value = (MInteger)mFloatValue;
  }
  else if (mType == MParamType::String)
  {
    try
    {
      value = std::stoi(mStringValue);
    }
    catch (std::invalid_argument& ex)
    {
      throw MEXCEPTION(MString("The value of the string does not cast to an integer: ") + ex.what());
    }
  }
  else if (mType == MParamType::Reference)
  {
    value = mReferenceValue->IntegerValue();
  }
  else if (mType == MParamType::Data)
  {
    throw MEXCEPTION("Cannot cast data type parameter to integer");
  }
  else
  {
    throw MEXCEPTION("Array type parameters cannot be cast to integer");
  }

  return value;
}

// --------------------------------------------------------------
//  BooleanValue
// --------------------------------------------------------------
// If possible, obtain the boolean equivalent value of the param

bool MParam::BooleanValue(void) const
{
  bool value = false;

  if (mType == MParamType::Invalid)
  {
    throw MEXCEPTION("Cannot cast an Invalid parameter to boolean");
  }
  else if (mType == MParamType::Exists)
  {
    value = true;
  }
  else if (mType == MParamType::Boolean || mType == MParamType::Integer)
  {
    value = (mIntegerValue != 0);
  }
  else if (mType == MParamType::Float)
  {
    value = (mFloatValue != 0);
  }
  else if (mType == MParamType::String)
  {
    if (mStringValue == "true" || mStringValue == "True" || mStringValue == "TRUE" || mStringValue == "1")
    {
      value = true;
    }
    else if (mStringValue == "false" || mStringValue == "False" || mStringValue == "FALSE" || mStringValue == "0")
    {
      value = false;
    }
    else
    {
      throw MEXCEPTION("Cannot cast string '" + mStringValue + "' to boolean.");
    }
  }
  else if (mType == MParamType::Reference)
  {
    value = mReferenceValue->BooleanValue();
  }
  else if (mType == MParamType::Data)
  {
    throw MEXCEPTION("Cannot cast data type parameter to boolean");
  }
  else
  {
    throw MEXCEPTION("Array type parameters cannot be cast to a boolean");
  }

  return value;
}

// --------------------------------------------------------------
//  FloatValue
// --------------------------------------------------------------
// If possible, obtain the float equivalent value of the param

MFloat MParam::FloatValue(void) const
{
  MFloat value = 0;

  if (mType == MParamType::Invalid)
  {
    throw MEXCEPTION("Cannot cast an Invalid parameter to Float");
  }
  else if (mType == MParamType::Exists)
  {
    value = 1.0;
  }
  else if (mType == MParamType::Float)
  {
    value = mFloatValue;
  }
  else if (mType == MParamType::Integer || mType == MParamType::Boolean)
  {
    value = (MFloat)mIntegerValue;
  }
  else if (mType == MParamType::String)
  {
    try
    {
      value = std::stod(mStringValue);
    }
    catch (std::invalid_argument& ex)
    {
      throw MEXCEPTION(MString("The value of the string does not cast to a floating point number: ") + ex.what());
    }
  }
  else if (mType == MParamType::Reference)
  {
    value = mReferenceValue->FloatValue();
  }
  else if (mType == MParamType::Data)
  {
    throw MEXCEPTION("Cannot cast data type parameter to floating point number");
  }
  else
  {
    throw MEXCEPTION("Array type parameters cannot be cast to a floating point number");
  }

  return value;
}

// --------------------------------------------------------------
//  IntegerArrayValue
// --------------------------------------------------------------
// If possible, obtain the integer vector equivalent value of the
// param

MVector<MInteger> MParam::IntegerArrayValue(void) const
{
  if (mType == MParamType::IntegerArray)
  {
    return mIntegersValue;
  }
  else if (mType == MParamType::Reference)
  {
    return mReferenceValue->IntegerArrayValue();
  }

  // TODO: Consider full array cast (i.e. floats to ints or strings to ints)

  // For other types, try to cast.
  MVector<MInteger> integers;
  try
  {
    integers.push_back(IntegerValue());
  }
  catch (std::exception& ex)
  {
    // Just return an empty list
  }
  return integers;
}

// --------------------------------------------------------------
//  FloatArrayValue
// --------------------------------------------------------------
// If possible, obtain the float vector equivalent value of the
// param

MVector<MFloat> MParam::FloatArrayValue(void) const
{
  if (mType == MParamType::FloatArray)
  {
    return mFloatsValue;
  }
  else if (mType == MParamType::Reference)
  {
    return mReferenceValue->FloatArrayValue();
  }


  // TODO: Consider full array cast (i.e. ints to floats or strings to floats)

  // For other types, try to cast.
  MVector<MFloat> floats;
  try
  {
    floats.push_back(FloatValue());
  }
  catch (std::exception& ex)
  {
    // Just return an empty list
  }
  return floats;
}

// --------------------------------------------------------------
//  StringArrayValue
// --------------------------------------------------------------
// If possible, obtain the string vector equivalent value of the
// param

MVector<MString> MParam::StringArrayValue(void) const
{
  if (mType == MParamType::StringArray)
  {
    return mStringsValue;
  }
  else if (mType == MParamType::Reference)
  {
    return mReferenceValue->StringArrayValue();
  }

  // TODO: Consider full array cast (i.e. ints to strings or floats to strings)

  // For other types, try to cast.
  MVector<MString> strings;
  try
  {
    strings.push_back(StringValue());
  }
  catch (std::exception& ex)
  {
    // Just return an empty list
  }
  return strings;
}

// --------------------------------------------------------------
//  PointerValue
// --------------------------------------------------------------
// If possible, obtain the string vector equivalent value of the
// param.  Failing, return the null pointer.

void* MParam::PointerValue(void) const
{
  void* ptr = kNull;

  if (mType == MParamType::Pointer)
  {
    ptr = mPointerValue;
  }
  else if (mType == MParamType::Reference)
  {
    ptr = mReferenceValue->PointerValue();
  }

  return ptr;
}

// --------------------------------------------------------------
//  ToBoolean
// --------------------------------------------------------------
// Convert this parameter to a boolean value.

void MParam::ToBoolean(void)
{
  if (mType == MParamType::Invalid)
  {
    throw MEXCEPTION("Cannot convert an Invalid parameter to boolean");
  }
  else if (mExpectedType != MParamType::Invalid && mType != MParamType::Boolean)
  {
    throw MEXCEPTION_BACKTRACE("Attempted to convert a non-boolean parameter to boolean.");
  }
  else if (mType == MParamType::Exists)
  {
    mType = MParamType::Boolean;
    mIntegerValue = 1;
  }
  else if (mType == MParamType::Integer)
  {
    mType = MParamType::Boolean;
    mIntegerValue = (mIntegerValue == 0 ? 0 : 1);
  }
  else if (mType == MParamType::Boolean)
  {
    // Nothing to do
  }
  else if (mType == MParamType::Float)
  {
    mType = MParamType::Boolean;
    mIntegerValue = (mFloatValue == 0 ? 0 : 1);
    mFloatValue = 0;
  }
  else if (mType == MParamType::String)
  {
    if (mStringValue == "true" || mStringValue == "True" || mStringValue == "TRUE" || mStringValue == "1")
    {
      mType = MParamType::Boolean;
      mIntegerValue = 1;
      mStringValue = kEmptyString;
    }
    else if (mStringValue == "false" || mStringValue == "False" || mStringValue == "FALSE" || mStringValue == "0")
    {
      mType = MParamType::Boolean;
      mIntegerValue = 0;
      mStringValue = kEmptyString;
    }
    else
    {
      throw MEXCEPTION("Cannot convert string '" + mStringValue + "' to boolean.");
    }
  }
  else if (mType == MParamType::Reference)
  {
    mReferenceValue->ToBoolean();
  }
  else if (mType == MParamType::Data)
  {
    throw MEXCEPTION("Cannot convert data type parameter to boolean");
  }
  else
  {
    throw MEXCEPTION("Array type parameters cannot be converted to a boolean");
  }
}

// --------------------------------------------------------------
//  ToBoolean
// --------------------------------------------------------------
// Convert this parameter to an integer value.

void MParam::ToInteger(void)
{
  if (mType == MParamType::Invalid)
  {
    throw MEXCEPTION("Cannot convert parameter of type Invalid to Integer");
  }
  else if (mExpectedType != MParamType::Invalid && mType != MParamType::Integer)
  {
    throw MEXCEPTION_BACKTRACE("Attempted to convert a non-integer parameter to integer.");
  }
  else if (mType == MParamType::Boolean)
  {
    mType = MParamType::Integer;
  }
  else if (mType == MParamType::Exists)
  {
    mType = MParamType::Integer;
    mIntegerValue = 1;
  }
  else if (mType == MParamType::Float)
  {
    mType = MParamType::Integer;
    mIntegerValue = (MInteger)mFloatValue;
    mFloatValue = 0;
  }
  else if (mType == MParamType::String)
  {
    try
    {
      mIntegerValue = std::stoi(mStringValue);
      mType = MParamType::Integer;
      mStringValue = kEmptyString;
    }
    catch (std::invalid_argument& ex)
    {
      throw MEXCEPTION(MString("The value of the string '") + mStringValue + "' cannot be converted to an integer: " + ex.what());
    }
  }
  else if (mType == MParamType::Reference)
  {
    mReferenceValue->ToInteger();
  }
  else if (mType == MParamType::Data)
  {
    throw MEXCEPTION("Cannot convert data type parameter to integer");
  }
  else if (mType == MParamType::FloatArray)
  {
    mIntegersValue.clear();
    for (auto iter = mFloatsValue.begin(); iter != mFloatsValue.end(); ++iter)
    {
      mIntegersValue.push_back((MInteger)*iter);
    }
    mFloatsValue.clear();
  }
  else if (mType == MParamType::StringArray)
  {
    mIntegersValue.clear();
    for (auto iter = mStringsValue.begin(); iter != mStringsValue.end(); ++iter)
    {
      try
      {
        MInteger intValue = std::stoi(*iter);
        mIntegersValue.push_back(intValue);
      }
      catch (std::invalid_argument& ex)
      {
        mIntegersValue.clear();
        throw MEXCEPTION(MString("The value of the string '") + *iter + "' cannot be converted to an integer: " + ex.what());
      }
    }

    mType = MParamType::IntegerArray;
    mStringsValue.clear();
  }
}

// --------------------------------------------------------------
//  ToBoolean
// --------------------------------------------------------------
// Convert this parameter to a float value.

void MParam::ToFloat(void)
{
  if (mType == MParamType::Invalid)
  {
    throw MEXCEPTION("Cannot convert parameter of type Invalid to Float");
  }
  else if (mExpectedType != MParamType::Invalid && mType != MParamType::Float)
  {
    throw MEXCEPTION_BACKTRACE("Attempted to convert a non-float parameter to float.");
  }
  else if (mType == MParamType::Exists)
  {
    mType = MParamType::Float;
    mFloatValue = 1.0;
  }
  else if (mType == MParamType::Boolean)
  {
    mType = MParamType::Float;
    mFloatValue = (MFloat)mIntegerValue;
    mIntegerValue = 0;
  }
  else if (mType == MParamType::Integer)
  {
    mType = MParamType::Float;
    mFloatValue = (MFloat)mIntegerValue;
    mIntegerValue = 0;
  }
  else if (mType == MParamType::String)
  {
    try
    {
      mFloatValue = std::stod(mStringValue);
      mType = MParamType::Float;
      mStringValue = kEmptyString;
    }
    catch (std::invalid_argument& ex)
    {
      throw MEXCEPTION(MString("The value of the string '") + mStringValue + "' cannot be converted to a float: " + ex.what());
    }
  }
  else if (mType == MParamType::Reference)
  {
    mReferenceValue->ToFloat();
  }
  else if (mType == MParamType::Data)
  {
    throw MEXCEPTION("Cannot convert data type parameter to floating point number");
  }
  else if (mType == MParamType::IntegerArray)
  {
    mFloatsValue.clear();
    for (auto iter = mIntegersValue.begin(); iter != mIntegersValue.end(); ++iter)
    {
      mFloatsValue.push_back((MInteger)*iter);
    }
    mIntegersValue.clear();
    mType = MParamType::FloatArray;
  }
  else if (mType == MParamType::StringArray)
  {
    mFloatsValue.clear();
    for (auto iter = mStringsValue.begin(); iter != mStringsValue.end(); ++iter)
    {
      try
      {
        MFloat floatValue = std::stod(*iter);
        mFloatsValue.push_back(floatValue);
      }
      catch (std::invalid_argument& ex)
      {
        mFloatsValue.clear();
        throw MEXCEPTION(MString("The value of the string '") + *iter + "' cannot be converted to a float: " + ex.what());
      }
    }

    mType = MParamType::FloatArray;
    mStringsValue.clear();
  }
}

// --------------------------------------------------------------
//  ToString
// --------------------------------------------------------------
// Convert this parameter to a string value.

void MParam::ToString(void)
{
  if (mType == MParamType::Invalid)
  {
    throw MEXCEPTION("Connot convert Invalid parameter to String");
  }
  else if (mExpectedType != MParamType::Invalid && mType != MParamType::String)
  {
    throw MEXCEPTION_BACKTRACE("Attempted to convert a non-string parameter to string.");
  }
  else if (mType == MParamType::Exists)
  {
    mType = MParamType::String;
    mStringValue = "";
  }
  else if (mType == MParamType::Boolean)
  {
    mType = MParamType::String;
    mStringValue = (mIntegerValue == 0 ? "false" : "true");
    mIntegerValue = 0;
  }
  else if (mType == MParamType::Integer)
  {
    mType = MParamType::String;
    mStringValue = UString::IntegerToString(mIntegerValue);
    mIntegerValue = 0;
  }
  else if (mType == MParamType::Float)
  {
    mType = MParamType::String;
    mStringValue = UString::FloatToString(mFloatValue);
    mFloatValue = 0;
  }
  else if (mType == MParamType::Data)
  {
    mType = MParamType::String;
    mStringValue = reinterpret_cast<const char*>(&mDataValue[0]);
    mDataValue.clear();
  }
  else if (mType == MParamType::IntegerArray)
  {
    mStringsValue.clear();
    for (auto it = mIntegersValue.begin(); it != mIntegersValue.end(); it++)
    {
      mStringsValue.push_back(UString::IntegerToString(*it));
    }
    mIntegersValue.clear();
    mType = MParamType::StringArray;
  }
  else if (mType == MParamType::FloatArray)
  {
    mStringsValue.clear();

    for (auto it = mFloatsValue.begin(); it != mFloatsValue.end(); it++)
    {
      mStringsValue.push_back(UString::FloatToString(*it));
    }
    mFloatsValue.clear();
    mType = MParamType::StringArray;
  }
  else if (mType == MParamType::Reference)
  {
    mReferenceValue->ToString();
  }
}

// --------------------------------------------------------------
//  CastsToBoolean
// --------------------------------------------------------------
// Whether or not this parameter can be cast to a boolean.

bool MParam::CastsToBoolean(void) const
{
  bool casts = true;

  try
  {
    BooleanValue();
  }
  catch (std::exception& ex)
  {
    casts = false;
  }

  return casts;
}

// --------------------------------------------------------------
//  CastsToInteger
// --------------------------------------------------------------
// Whether or not this parameter can be cast to an integer.

bool MParam::CastsToInteger(void) const
{
  bool casts = true;

  try
  {
    IntegerValue();
  }
  catch (std::exception& ex)
  {
    casts = false;
  }

  return casts;
}

// --------------------------------------------------------------
//  CastsToFloat
// --------------------------------------------------------------
// Whether or not this parameter can be cast to a float.

bool MParam::CastsToFloat(void) const
{
  bool casts = true;

  try
  {
    FloatValue();
  }
  catch (std::exception& ex)
  {
    casts = false;
  }

  return casts;
}

// --------------------------------------------------------------
//  Evaluate
// --------------------------------------------------------------
// Using a string escaper, evaluate the parameter using its
// string value.

MString MParam::Evaluate(MStringEscaper* inEscaper) const
{
  MString value;

  if (mType == MParamType::Invalid)
  {
    throw MEXCEPTION("Cannot evaluate an invalid parameter.");
  }

  if (inEscaper)
  {
    if (mType == MParamType::String)
    {
      value = inEscaper->Escape(mStringValue);
    }
    else if (mType == MParamType::StringArray)
    {
      for (auto iter = mStringsValue.begin(); iter != mStringsValue.end(); ++iter)
      {
        if (value.empty())
        {
          value = inEscaper->Escape(*iter);
        }
        else
        {
          value += inEscaper->Separator() + inEscaper->Escape(*iter);
        }
      }
    }
    else if (mType == MParamType::Reference)
    {
      value = mReferenceValue->Evaluate(inEscaper);
    }
    else
    {
      value = StringValue();
    }
  }
  else
  {
    value = StringValue();
  }

  return value;
}

// --------------------------------------------------------------
//  Modified
// --------------------------------------------------------------
// Take action upon modifying this parameter.

void MParam::Modified(void)
{
  if (mModified)
  {
    BroadcastMessage(kMsg_Modified, this);
    mModified = false;
  }
}

// Left-hand comparison operator definitions
bool operator<(const MFloat& inValue, const MParam& inParam) { return inParam > inValue; }
bool operator<=(const MFloat& inValue, const MParam& inParam) { return inParam >= inValue; }
bool operator>(const MFloat& inValue, const MParam& inParam) { return inParam < inValue; }
bool operator>=(const MFloat& inValue, const MParam& inParam) { return inParam <= inValue; }
bool operator==(const MFloat& inValue, const MParam& inParam) { return inParam == inValue; }
bool operator!=(const MFloat& inValue, const MParam& inParam) { return inParam != inValue; }
