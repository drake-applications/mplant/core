/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MLogger.h"
#include "UDateTime.h"
#include "UString.h"
#include <fstream>
#include <iostream>
#include <ctime>

// --------------------------------------------------------------
//  Constructors
// --------------------------------------------------------------

MLogger::MLogger(MLogger* inParent) : MConcurrent()
{
  SetParent(inParent);
}

MLogger::MLogger(const MString& inName, MLogger* inParent) : MConcurrent()
{
  mName = inName;
  SetParent(inParent);
}

// --------------------------------------------------------------
//  Destructor
// --------------------------------------------------------------

MLogger::~MLogger(void)
{
  for (MVector<MStreamSpec>::iterator iter = mDestinations.begin(); iter != mDestinations.end(); ++iter)
  {
    if ((*iter).mOwnsStream)
    {
      delete (*iter).mStream;
    }
  }
}

// --------------------------------------------------------------
//  SetParent
// --------------------------------------------------------------
// Set the parent logger.

void MLogger::SetParent(MLogger* inLogger)
{
  mParent = inLogger;
  Inherit();
}

// --------------------------------------------------------------
//  Inherit
// --------------------------------------------------------------
// Inherit certain properties from the parent logger.

void MLogger::Inherit(void)
{
  if (mParent)
  {
    mLogLevel = mParent->mLogLevel;
    mMinLogLevel = mParent->mMinLogLevel;
  }
  else
  {
    mMinLogLevel = mLogLevel;
  }
}

// --------------------------------------------------------------
//  AddDestination
// --------------------------------------------------------------
// This function adds a logging destination to the logger.

void MLogger::AddDestination(const MString& inFile, const MString& inFormat, const MLogLevel& inLogLevel)
{
  MStreamSpec spec;
  spec.mOwnsStream = false;
  spec.mFormat = inFormat;
  spec.mLogLevel = inLogLevel;

  mMinLogLevel = M_MIN(mMinLogLevel, inLogLevel);

  if (inFile == "stderr" || inFile == "cerr")
  {
    spec.mStream = &std::cerr;
  }
  else if (inFile == "stdout" || inFile == "cout")
  {
    spec.mStream = &std::cout;
  }
  else if (inFile == "clog")
  {
    spec.mStream = &std::clog;
  }
  else
  {
    spec.mStream = new std::ofstream(inFile, std::ios_base::app);
    spec.mOwnsStream = true;
  }
  mDestinations.push_back(spec);
}

// --------------------------------------------------------------
//  AddDefaultDestination
// --------------------------------------------------------------
// Shortcut for a simple stderr logging destination.

void MLogger::AddDefaultDestination(void)
{
  AddDestination("cerr", "%d [%p] %N: %m");
}

// --------------------------------------------------------------
//  EnterLog
// --------------------------------------------------------------
// This function prints a message to the log destinations.

void MLogger::EnterLog(const MLogLevel& inLevel, const MString& inComment, const MString& inChildName)
{
  if (mParent)
  {
    mParent->EnterLog(inLevel, inComment, (inChildName.empty() ? mName : mName + "::" + inChildName));
  }
  if (mDestinations.size() > 0)
  {
    WriteToLog(inLevel, inComment, inChildName);
  }
}

// --------------------------------------------------------------
//  EnterLog
// --------------------------------------------------------------
// This function prints a message to the log destinations.

void MLogger::WriteToLog(const MLogLevel& inLevel, const MString& inComment, const MString& inSource)
{
  MString source;
  MString level;

  if (inSource.empty())
  {
    source = mName;
  }
  else
  {
    source = mName + "::" + inSource;
  }

  if (inLevel == MLogLevel::Fatal)
  {
    level = "FATAL";
  }
  else if (inLevel == MLogLevel::Error)
  {
    level = "ERROR";
  }
  else if (inLevel == MLogLevel::Warn)
  {
    level = " WARN";
  }
  else if (inLevel == MLogLevel::Inform)
  {
    level = " INFO";
  }
  else if (inLevel == MLogLevel::Trace)
  {
    level = "TRACE";
  }
  else if (inLevel == MLogLevel::Debug)
  {
    level = "DEBUG";
  }

  for (MVector<MStreamSpec>::iterator iter = mDestinations.begin(); iter != mDestinations.end(); ++iter)
  {
    MLogLevel destLogLevel = (*iter).mLogLevel;
    if (destLogLevel == MLogLevel::NotSet)
    {
      destLogLevel = mLogLevel;
    }

    if (destLogLevel <= inLevel)
    {
      MString logEntry = (*iter).mFormat;
      logEntry = UString::Replace(logEntry, "%p", level);
      logEntry = UString::Replace(logEntry, "%N", source);
      logEntry = UString::Replace(logEntry, "%d", UDateTime::FormatNow());
      logEntry = UString::Replace(logEntry, "%m", inComment);

      MStdUniqueLock lock(mMutex, std::defer_lock);
      if (GetLock(lock))
      {
        (*(*iter).mStream) << logEntry << std::endl;
        lock.unlock();
        SetConditionMet();
      }
    }
  }
}

// --------------------------------------------------------------
//  ShouldLog
// --------------------------------------------------------------
// This function determines whether logging would occur at all
// for the given level.

bool MLogger::ShouldLog(const MLogLevel& inLevel)
{
  return (mMinLogLevel <= inLevel);
}

// --------------------------------------------------------------
//  Fatal
// --------------------------------------------------------------
// This function prints a fatal error to the target destinations.

void MLogger::Fatal(const MString& inComment)
{
  if (ShouldLog(MLogLevel::Fatal))
  {
    EnterLog(MLogLevel::Fatal, inComment);
  }
}

// --------------------------------------------------------------
//  Error
// --------------------------------------------------------------
// This function prints an error to the target destinations.

void MLogger::Error(const MString& inComment)
{
  if (ShouldLog(MLogLevel::Error))
  {
    EnterLog(MLogLevel::Error, inComment);
  }
}

// --------------------------------------------------------------
//  Warn
// --------------------------------------------------------------
// This function prints a warning to the target destinations.

void MLogger::Warn(const MString& inComment)
{
  if (ShouldLog(MLogLevel::Warn))
  {
    EnterLog(MLogLevel::Warn, inComment);
  }
}

// --------------------------------------------------------------
//  Inform
// --------------------------------------------------------------
// This function prints info to the target destinations.

void MLogger::Inform(const MString& inComment)
{
  if (ShouldLog(MLogLevel::Inform))
  {
    EnterLog(MLogLevel::Inform, inComment);
  }
}

// --------------------------------------------------------------
//  Trace
// --------------------------------------------------------------
// This function prints trace info to the target destinations.

void MLogger::Trace(const MString& inComment)
{
  if (ShouldLog(MLogLevel::Trace))
  {
    EnterLog(MLogLevel::Trace, inComment);
  }
}

// --------------------------------------------------------------
//  Debug
// --------------------------------------------------------------
// This function prints a debug message to the target destinations.

void MLogger::Debug(const MString& inComment)
{
  if (ShouldLog(MLogLevel::Debug))
  {
    EnterLog(MLogLevel::Debug, inComment);
  }
}

// --------------------------------------------------------------
//  Debug
// --------------------------------------------------------------
// This function tells the caller whether anything would actually
// be logged if a Debug call were made.  Use this to preempt any
// expensive activities that might be undertaken to produce the
// debug message, if the message will not even be logged.

bool MLogger::Debug(void)
{
  return ShouldLog(MLogLevel::Debug);
}

// --------------------------------------------------------------
//  ReadLogLevel  [static]
// --------------------------------------------------------------
// This function converts a string to a log level

MLogLevel MLogger::ReadLogLevel(const MString& inLevelStr)
{
  MLogLevel logLevel = MLogLevel::NotSet;
  MString levelStr = UString::ToUpper(inLevelStr);

  if (levelStr == "ERROR")
  {
    logLevel = MLogLevel::Error;
  }
  else if (levelStr == "WARN")
  {
    logLevel = MLogLevel::Warn;
  }
  else if (levelStr == "INFO" || levelStr == "INFORM")
  {
    logLevel = MLogLevel::Inform;
  }
  else if (levelStr == "TRACE")
  {
    logLevel = MLogLevel::Trace;
  }
  else if (levelStr == "DEBUG")
  {
    logLevel = MLogLevel::Debug;
  }

  return logLevel;
}
