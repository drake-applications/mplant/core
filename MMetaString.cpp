/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Historical Note
 *
 * The original MetaString parser on which this class is based was
 * written in Objective-C while I was at Paladin Logic.  It was used in
 * several iOS Apps.  I later ported it to VisualBasic for another
 * Paladin project.  I received express permission to port the parser to
 * C++ and again to Java, crediting Paladin Logic as a contributor.  I
 * have improved the parser with each port.
 *
 * For more information about Paladin Logic, see www.paladinlogic.com.
 */

#include "MMetaString.h"
#include "UString.h"
#include <stdexcept>

// --------------------------------------------------------------
//  MMetaStringLexicon Constructor
// --------------------------------------------------------------
// Define the default lexicon of symbols and keywords for
// MMetaStrings.

MMetaStringLexicon::MMetaStringLexicon(void) : MLexicon()
{
  mStartTag = "<?";
  mEndTag = "?>";
  mStartFunction = "(";
  mEndFunction = ")";
  mQuote = "\"";
  mAltQuote = "\'";
  mIf = "if";
  mElse = "else";
  mElseIf = "elseif";
  mEnd = "end";
  mNot = "not";
  mExists = "exists";
  mLiteral = "literal";
  mValue = "value";
}

// --------------------------------------------------------------
//  MMetaString Constructors
// --------------------------------------------------------------

MMetaString::MMetaString(MMetaStringLexicon* inLexicon)
{
  SetLexicon(inLexicon);
}

MMetaString::MMetaString(const char* inStr, MMetaStringLexicon* inLexicon)
{
  SetLexicon(inLexicon);
  SetString(MString(inStr));
}

MMetaString::MMetaString(const MString& inString, MMetaStringLexicon* inLexicon)
{
  SetLexicon(inLexicon);
  SetString(inString);
}

// --------------------------------------------------------------
//  SetLexicon
// --------------------------------------------------------------
// Set the lexicon for the given meta string.

void MMetaString::SetLexicon(MMetaStringLexicon* inLexicon)
{
  mLexicon = inLexicon;
  if (!mLexicon)
  {
    // Use the default lexicon, if none is provided
    mLexicon = new MMetaStringLexicon();
  }
}

// --------------------------------------------------------------
//  MMetaString Destructor
// --------------------------------------------------------------
// Delete meta blocks (TODO: Use smart pointers?)

MMetaString::~MMetaString()
{
  for (auto iter = mBlocks.begin(); iter != mBlocks.end(); ++iter)
  {
    delete (*iter);
  }
  mBlocks.clear();
}

// --------------------------------------------------------------
//  SetString
// --------------------------------------------------------------
// Assign a new value to the meta string and parse it into its
// constituent blocks.

void MMetaString::SetString(const MString& inString)
{
  mBlocks.clear();
  ParseString(inString);
}

// --------------------------------------------------------------
//  Evaluate
// --------------------------------------------------------------
// Evalutate the meta string with the given parameters.

MString MMetaString::Evaluate(const MParamMap& inParameters, MStringEscaper* inEscaper) const
{
  MParamMap emptyOverrideParams;
  return Evaluate(inParameters, emptyOverrideParams, inEscaper);
}

// --------------------------------------------------------------
//  Evaluate
// --------------------------------------------------------------
// Evalutate the meta string with the given parameters, including
// overriding parametrs.

MString MMetaString::Evaluate(const MParamMap& inParameters, const MParamMap& inOverrideParams, MStringEscaper* inEscaper) const
{
  MString evaluatedString;

  try
  {
    for (MVector<MMetamorphicBlock*>::const_iterator iter = mBlocks.begin(); iter != mBlocks.end(); iter++)
    {
      (*iter)->Evaluate(evaluatedString, inParameters, inOverrideParams, mLexicon, inEscaper);
    }
  }
  catch (std::exception& e)
  {
    throw std::runtime_error(MString("MMetaString Evaluation Error: ") + e.what());
  }

  return evaluatedString;
}

// --------------------------------------------------------------
//  ParseString
// --------------------------------------------------------------
// Parse the given string into meta blocks for later evaluation.

void MMetaString::ParseString(const MString& inString)
{
  MMetamorphicBlock* block = kNull;
  MIndex index = 0;
  MSize stringLength = inString.length();
  MSize scanLength = stringLength;

  do
  {
    block = ReadNextBlock(inString, index, scanLength, kNull);
    if (block)
    {
      mBlocks.push_back(block);
    }
    scanLength = stringLength - index;
  }
  while (index < stringLength);
}

// --------------------------------------------------------------
//  ReadNextBlock
// --------------------------------------------------------------
// Read the next meta block in the string (called by parser).

MMetamorphicBlock* MMetaString::ReadNextBlock(const MString& inString, MIndex& ioIndex, const MSize& inScanLength, MMetamorphicBlock* inParentBlock)
{
  MMetamorphicBlock* block = kNull;
  MMetamorphicBlock* parent = kNull;
  MMetamorphicBlock* newBlock = kNull;
  MMetamorphicTag* tag = kNull;

  MIndex start = kNotFound;
  MSize startLength = 0;
  MSize stringLength = inString.length();

  if (inScanLength > 0)
  {
    start = UString::Find(inString, mLexicon->GetStartTag(), ioIndex, inScanLength);

    if (start == kNotFound)
    {
      block = new MMetamorphicBlock(inString.substr(ioIndex, inScanLength));
      ioIndex += inScanLength;
    }
    else if (start > ioIndex)
    {
      startLength = start - ioIndex;
      block = new MMetamorphicBlock(inString.substr(ioIndex, startLength));
      ioIndex += startLength;
    }
    else
    {
      tag = ReadNextTag(inString, ioIndex, inScanLength);
      startLength = stringLength - ioIndex;

      if (tag->IsEndOfBlock())
      {
        if (inParentBlock)
        {
          inParentBlock->AddTag(tag);
        }
        else
        {
          throw std::runtime_error("MMetaString Parse Error:  Closing tag found before opening tag.");
        }
      }

      if (tag->IsStartOfBlock())
      {
        block = new MMetamorphicBlock();
        if (!tag->IsEndOfBlock())
        {
          block->AddTag(tag);
          parent = block;
        }
        else
        {
          parent = inParentBlock;
        }

        do
        {
          newBlock = ReadNextBlock(inString, ioIndex, startLength, parent);
          if (newBlock)
          {
            tag->AddBlock(newBlock);
          }
          startLength = stringLength - ioIndex;
        }
        while (newBlock);
      }

      if (!tag->IsStartOfBlock() && !tag->IsEndOfBlock())
      {
        block = new MMetamorphicBlock();
        block->AddTag(tag);
      }
    }
    if (block && block->IsEmpty())
    {
      delete block;
      block = kNull;
    }
  }
  return block;
}

// --------------------------------------------------------------
//  ReadNextTag
// --------------------------------------------------------------
// Read the next meta tag in the block.

MMetamorphicTag* MMetaString::ReadNextTag(const MString& inString, MIndex& ioIndex, const MSize& inScanLength)
{
  MString startTag = mLexicon->GetStartTag();
  MString endTag = mLexicon->GetEndTag();
  MString startFunction = mLexicon->GetStartFunction();
  MString endFunction = mLexicon->GetEndFunction();
  MString ifWord = mLexicon->GetIf();
  MString elseWord = mLexicon->GetElse();
  MString endWord = mLexicon->GetEnd();
  MString literalWord = mLexicon->GetLiteral();
  MString valueWord = mLexicon->GetValue();

  MSize tagMarkerLength = startTag.length();

  MMetamorphicTag* tag = kNull;
  MIndex tagStartPosition = UString::Find(inString, startTag, ioIndex, inScanLength);
  MIndex tagEndPosition = UString::Find(inString, endTag, ioIndex, inScanLength);
  MSize tagLength = 0;

  MIndex searchPosition = 0;
  MSize searchLength = 0;

  MIndex operatorStartPosition = 0;
  MIndex operatorEndPosition = 0;

  MString tagString;
  MString tagType;
  MString parameter;

  bool isLogicalStart = false;
  bool isLogicalEnd = false;

  if (tagStartPosition != kNotFound)
  {

    if (tagEndPosition == kNotFound)
    {
      throw std::runtime_error("MMetaString Parse Error: Missing end tag " + endTag);
    }
    if (tagStartPosition >= tagEndPosition)
    {
      throw std::runtime_error("MMetaString Parse Error: " + endTag + " precedes " + startTag);
    }

    tagEndPosition += tagMarkerLength;
    tagLength = tagEndPosition - tagStartPosition;

    searchPosition = tagStartPosition;
    searchLength = tagLength;

    tagString = inString.substr(searchPosition, searchLength);

    operatorStartPosition = tagMarkerLength;
    operatorEndPosition = UString::Find(tagString, startFunction);

    if (operatorEndPosition == kNotFound)
    {
      operatorEndPosition = tagLength - tagMarkerLength;
    }

    tagType = UString::Trim(tagString.substr(operatorStartPosition, operatorEndPosition - operatorStartPosition));

    if (UString::Find(tagType, ifWord) != kNotFound || UString::Find(tagType, elseWord) != kNotFound)
    {
      if (UString::Find(tagType, endWord) != kNotFound)
      {
        isLogicalEnd = true;
      }
      else if (UString::Find(tagType, ifWord) != kNotFound)
      {
        isLogicalStart = true;
        parameter = ReadTagParameter(tagString);
      }

      if (UString::Find(tagType, elseWord) != kNotFound)
      {
        isLogicalStart = true;
        isLogicalEnd = true;
      }
    }
    else if (tagType == literalWord || tagType == valueWord)
    {
      parameter = ReadTagParameter(tagString);
    }

    tag = new MMetamorphicTag(tagType, isLogicalStart, isLogicalEnd, parameter);
    ioIndex += tagLength;
  }

  return tag;
}

// --------------------------------------------------------------
//  ReadTagParameter
// --------------------------------------------------------------
// Read the kind of tag given.

MString MMetaString::ReadTagParameter(const MString& inTagString)
{
  MString quote = mLexicon->GetQuote();
  MString altQuote = mLexicon->GetAltQuote();

  MString parameter;
  MIndex quoteOpenPosition;
  MIndex quoteClosePosition;

  quoteOpenPosition = UString::Find(inTagString, quote);
  quoteClosePosition = UString::ReverseFind(inTagString, quote);

  if (quoteOpenPosition == kNotFound)
  {
    quoteOpenPosition = UString::Find(inTagString, altQuote);
    quoteClosePosition = UString::ReverseFind(inTagString, altQuote);
  }

  if (quoteOpenPosition != kNotFound && quoteClosePosition != kNotFound && quoteClosePosition > quoteOpenPosition)
  {
    parameter = inTagString.substr(quoteOpenPosition + 1, quoteClosePosition - quoteOpenPosition - 1);
  }
  else
  {
    throw std::runtime_error("MMetaString Parse Error: Malformed tag - cannot read parameter: " + inTagString);
  }

  return parameter;
}

// MMetamorphicBlock

// --------------------------------------------------------------
//  MMetamorphicBlock Constructors
// --------------------------------------------------------------

MMetamorphicBlock::MMetamorphicBlock(void)
{

}

MMetamorphicBlock::MMetamorphicBlock(const MString& inBlock)
{
  mBlock = inBlock;
}

// --------------------------------------------------------------
//  MMetamorphicBlock Destructor
// --------------------------------------------------------------

MMetamorphicBlock::~MMetamorphicBlock(void)
{
  for (MMetamorphicTagVector::iterator iter = mTags.begin(); iter != mTags.end(); ++iter)
  {
    delete (*iter);
  }
  mTags.clear();
}

// --------------------------------------------------------------
//  AddTag
// --------------------------------------------------------------
// Record a new tag in the block.

void MMetamorphicBlock::AddTag(MMetamorphicTag* inTag)
{
  mTags.push_back(inTag);
}

// --------------------------------------------------------------
//  IsEmpty
// --------------------------------------------------------------
// Check whether the block and its tags are both empty.

bool MMetamorphicBlock::IsEmpty(void)
{
  return mBlock.empty() && mTags.empty();
}

// --------------------------------------------------------------
//  Evaluate
// --------------------------------------------------------------
// Evaluate the current block with the given parameters, per the
// supplied lexicon.

bool MMetamorphicBlock::Evaluate(MString& outString, const MParamMap& inParameters, const MParamMap& inOverrideParams, const TPointer<MMetaStringLexicon>& inLexicon, MStringEscaper* inEscaper)
{
  bool evaluated = false;

  if (mBlock.length() > 0)
  {
    outString += mBlock;
  }

  for (MMetamorphicTagVector::iterator iter = mTags.begin(); iter != mTags.end(); iter++)
  {
    MMetamorphicTag* tag = *iter;
    if (!(evaluated && tag->IsEndOfBlock()))
    {
      evaluated = tag->Evaluate(outString, inParameters, inOverrideParams, inLexicon, inEscaper);
    }
  }

  return evaluated;
}

// MMetamorphicTag

// --------------------------------------------------------------
//  MMetamorphicTag Constructor
// --------------------------------------------------------------

MMetamorphicTag::MMetamorphicTag(const MString& inName, bool isStart, bool isEnd, const MString& inParameter)
{
  mName = inName;
  mParameter = inParameter;
  mStartOfBlock = isStart;
  mEndOfBlock = isEnd;
}

// --------------------------------------------------------------
//  MMetamorphicTag Destructor
// --------------------------------------------------------------

MMetamorphicTag::~MMetamorphicTag(void)
{
  for (MMetamorphicBlockVector::iterator iter = mBlocks.begin(); iter != mBlocks.end(); ++iter)
  {
    delete (*iter);
  }
  mBlocks.clear();
}

// --------------------------------------------------------------
//  AddBlock
// --------------------------------------------------------------
// Record a new block subordinate to this meta tag.

void MMetamorphicTag::AddBlock(MMetamorphicBlock* inBlock)
{
  mBlocks.push_back(inBlock);
}

// --------------------------------------------------------------
//  Evaluate
// --------------------------------------------------------------
// Evaluate this tag based on the supplied parameters, per the
// given lexicon.

bool MMetamorphicTag::Evaluate(MString& outString, const MParamMap& inParameters, const MParamMap& inOverrideParams, const TPointer<MMetaStringLexicon>& inLexicon, MStringEscaper* inEscaper)
{
  MString ifExists = inLexicon->GetIf() + " " + inLexicon->GetExists();
  MString elseIfExists = inLexicon->GetElseIf() + " " + inLexicon->GetExists();
  MString ifNotExists = inLexicon->GetIf() + " " + inLexicon->GetNot() + " " + inLexicon->GetExists();
  MString elseIfNotExists = inLexicon->GetElseIf() + " " + inLexicon->GetNot() + " " + inLexicon->GetExists();
  MString elseWord = inLexicon->GetElse();
  MString literalWord = inLexicon->GetLiteral();
  MString valueWord = inLexicon->GetValue();

  bool evaluated = false;
  bool conditionMet = false;

  MParamMap::const_iterator paramIter = inParameters.find(mParameter);
  MParamMap::const_iterator overrideParamIter = inOverrideParams.find(mParameter);

  if (mName == ifExists || mName == elseIfExists)
  {
    conditionMet = paramIter != inParameters.end() || overrideParamIter != inOverrideParams.end();
  }
  else if (mName == ifNotExists || mName == elseIfNotExists)
  {
    conditionMet = paramIter == inParameters.end() && overrideParamIter == inOverrideParams.end();
  }
  else if (mName == elseWord)
  {
    conditionMet = true;
  }
  else if (mName == literalWord)
  {
    if (overrideParamIter != inOverrideParams.end())
    {
      outString += overrideParamIter->second.StringValue();
    }
    else if (paramIter != inParameters.end())
    {
      outString += paramIter->second.StringValue();
    }
    evaluated = true;
  }
  else if (mName == valueWord)
  {
    if (overrideParamIter != inOverrideParams.end())
    {
      outString += overrideParamIter->second.Evaluate(inEscaper);
    }
    else if (paramIter != inParameters.end())
    {
      outString += paramIter->second.Evaluate(inEscaper);
    }
    else
    {
      outString += inEscaper->NullValue();
    }
    evaluated = true;
  }
  // TODO "foreach"
  

  if (conditionMet)
  {
    for (MVector<MMetamorphicBlock*>::iterator iter = mBlocks.begin(); iter != mBlocks.end(); iter++)
    {
      (*iter)->Evaluate(outString, inParameters, inOverrideParams, inLexicon, inEscaper);
    }
    evaluated = true;
  }

  return evaluated;
}

