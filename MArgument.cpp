/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MArgument.h"
#include "UString.h"
#include <sstream>

// --------------------------------------------------------------
//  Constructors
// --------------------------------------------------------------

MArgument::MArgument(void)
{
  mType = MParamType::Invalid;
}

MArgument::MArgument(const MArgument& inArgument)
{
  mName = inArgument.mName;
  mShort = inArgument.mShort;
  mLong = inArgument.mLong;
  mPrompt = inArgument.mPrompt;
  mHelp = inArgument.mHelp;
  mNargs = inArgument.mNargs;
  mNargPattern = inArgument.mNargPattern;
  mType = inArgument.mType;
  mRequired = inArgument.mRequired;
  mHasMin = inArgument.mHasMin;
  mHasMax = inArgument.mHasMax;
  mMin = inArgument.mMin;
  mMax = inArgument.mMax;
  mParameter = inArgument.mParameter;
  mOptionalPrompt = inArgument.mOptionalPrompt;
  mContingent = inArgument.mContingent;
  mDefers = inArgument.mDefers;
  mHasDefault = inArgument.mHasDefault;
  mDefault = inArgument.mDefault;
  mOptions = inArgument.mOptions;
}

MArgument::MArgument(const MString& inName, const MSize& inNargs)
{
  // Positional 
  mName = inName;
  mNargs = inNargs;
  mNargPattern = MNargs::None;
  mType = MParamType::String;
  mRequired = true;
  mHasMin = false;
  mHasMax = false;
  mDefers = false;
  mHasDefault = false;
}

MArgument::MArgument(const MString& inName, const MNargs& inNargs)
{
  // Final
  mName = inName;
  mNargPattern = inNargs;
  mType = MParamType::String;
  mRequired = true;
  mHasMin = false;
  mHasMax = false;
  mDefers = false;
  mHasDefault = false;
}

MArgument::MArgument(const MString& inShort, const MString& inLong, const MSize& inNargs, bool inRequired)
{
  mShort = UString::TrimLeft(inShort, '-');
  mLong = UString::Trim(inLong, '-');
  mNargs = inNargs;
  mNargPattern = MNargs::None;
  mType = MParamType::String;
  mRequired = inRequired;
  mHasMin = false;
  mHasMax = false;
  mDefers = false;
  mHasDefault = false;
}

MArgument::MArgument(const MString& inShort, const MString& inLong, const MNargs& inNargs, bool inRequired)
{
  mShort = UString::Trim(inShort, '-');
  mLong = UString::Trim(inLong, '-');
  mNargPattern = inNargs;
  mNargs = -1;
  mType = MParamType::String;
  mRequired = inRequired;
  mHasMin = false;
  mHasMax = false;
  mDefers = false;
  mHasDefault = false;
}

// --------------------------------------------------------------
//  Destructor
// --------------------------------------------------------------
// 

MArgument::~MArgument(void)
{

}

// --------------------------------------------------------------
//  Read
// --------------------------------------------------------------
// Have the argument read itself out of the given argument list.

void MArgument::Read(int& ioIndex, const int& inCount, const char** argv, bool inExplicitFinal)
{
  MString error;
  bool nextOption = false;
  MVector<MString> args;

  // Read this argument's parameters in mParam
  if (mNargPattern != MNargs::None)
  {
    while (ioIndex < inCount && !nextOption)
    {
      MString value = argv[ioIndex];
      if (!inExplicitFinal && UString::StartsWith(value, "-"))
      {
        nextOption = true;
      }
      else
      {
        args.push_back(value);
        ++ioIndex;
      }
    }

    if (mNargPattern == MNargs::Plus && args.empty())
    {
      error = "Not enough arguments for option " + GetArgumentName();
      throw std::runtime_error(error);
    }

    if (args.size() == 1)
    {
      mParameter = (MString)args[0];
    }
    else
    {
      mParameter = args;
    }
  }
  else
  {
    MIndex i = 0;

    while (ioIndex < inCount && i < mNargs && !nextOption)
    {
      MString value = argv[ioIndex];
      if (!inExplicitFinal && UString::StartsWith(value, "-"))
      {
        nextOption = true;
      }
      else
      {
        args.push_back(value);
        ++ioIndex;
        ++i;
      } 
    }

    if (i < mNargs)
    {
      error = "Not enough arguments for option " + GetArgumentName();
      throw std::runtime_error(error);
    }

    if (args.size() == 1)
    {
      mParameter = (MString)args[0];
    }
    else
    {
      mParameter = args;
    }
  }
}

// --------------------------------------------------------------
//  Validate
// --------------------------------------------------------------
// Verify that the value given falls within any constraints that
// are defined for the argument.

void MArgument::Validate(void)
{
  // Make sure that the value(s) in mParam satisfy all constraints
  if (mType == MParamType::Integer || mType == MParamType::IntegerArray)
  {
    mParameter.ToInteger();
  }
  else if (mType == MParamType::Float || mType == MParamType::FloatArray)
  {
    mParameter.ToFloat();
  }

  if (mHasMin || mHasMax) 
  {
    if ((mType == MParamType::Integer ||
         mType == MParamType::IntegerArray ||
         mType == MParamType::Float ||
         mType == MParamType::FloatArray))
    {
      if ((mHasMin && mMin > mParameter) ||
          (mHasMax && mMax < mParameter))
      {
        throw std::runtime_error("Value of argument '" + GetArgumentName() + "' is out of range.");
      }
    }
    else
    {
      throw std::runtime_error("Programming Error: Argument '" + GetArgumentName() + "' with non-numeric type cannot have a min or max.");
    }
  }

  if (!mOptions.empty())
  {
    if (!mParameter.In(mOptions))
    {
      throw std::runtime_error("Value of argument '" + GetArgumentName() + "' does not match any listed option.");
    }
  }
}

// --------------------------------------------------------------
//  GetArgumentName
// --------------------------------------------------------------
// Access this argument's name.  If there isn't one, use the long
// name, and failing this, use the short name.

MString MArgument::GetArgumentName(void) const
{
  MString argName;

  if (!mName.empty())
  {
    argName = mName;
  }
  else if (HasLong())
  {
    argName = mLong;
  }
  else if (HasShort())
  {
    argName = mShort;
  }
  return argName;
}

// --------------------------------------------------------------
//  Usage
// --------------------------------------------------------------
// Print usage information for this argument.

MString MArgument::Usage(void)
{
  bool hasLong = HasLong();
  bool hasShort = HasShort();
  bool isOption = hasLong || hasShort;
  bool hasArg = mNargs > 0 || mNargPattern != MNargs::None;
  MString paramName = UString::Replace(UString::ToUpper(GetArgumentName()), " ", "_");

  std::ostringstream stream;
  if (!mRequired)
  {
    stream << "[";
  }
  if (hasShort && hasLong)
  {
    stream << "-" << mShort << " --" << mLong;
  }
  else if (hasShort)
  {
    stream << "-" << mShort;
  }
  else if (hasLong)
  {
    stream << "--" << mLong;
  }
  if (isOption && hasArg)
  {
    stream << " ";
  }
  if (mNargPattern == MNargs::Star)
  {
    stream << "...";
  }
  else if (mNargPattern == MNargs::Plus)
  {
    stream << paramName << "...";
  }
  else if (mNargs > 0)
  {
    for (MSize i = 0; i < mNargs; ++i)
    {
      stream << paramName;
      if (i < mNargs - 1)
      {
        stream << " ";
      }
    }
  }
  if (mType == MParamType::Integer)
  {
    stream << " as integer";
  }
  else if (mType == MParamType::Float)
  {
    stream << " as float";
  }
  if (mHasMin || mHasMax)
  {
    stream << " {";
    if (mHasMin)
    {
      stream << mMin;
    }
    else
    {
      stream << "?";
    }
    stream << "..";
    if (mHasMax)
    {
      stream << mMax;
    }
    else
    {
      stream << "?";
    }
    stream << "}";
  }
  if (!mOptions.empty())
  {
    stream << " {";
    for (auto iter = mOptions.begin(); iter != mOptions.end(); ++iter)
    {
      if (iter != mOptions.begin())
      {
        stream << " | ";
      }
      stream << (*iter).StringValue();
    }
    stream << "}";
  }
  if (mHasDefault)
  {
    stream << " default " << mDefault;
  }
  if (!mHelp.empty())
  {
    stream << " (" << mHelp << ")";
  }
  if (!mRequired)
  {
    stream << "]";
  }
  return stream.str();
}

// --------------------------------------------------------------
//  operator==
// --------------------------------------------------------------
// Compare two arguments by name.

bool MArgument::operator==(const MArgument& inArgument)
{
  return (inArgument.GetArgumentName() == GetArgumentName());
}
