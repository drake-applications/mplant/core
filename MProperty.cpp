/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MProperty.h"

MProperty::MProperty(void)
{
  mName = "undefined";
  mModified = false;
  mParam.AddListener(this);
}

MProperty::MProperty(const MString& inName)
{
  mName = inName;
  mModified = false;
  mParam.AddListener(this);
}

MProperty::MProperty(const MString& inName, const MParam& inParam)
{
  mName = inName;
  mModified = false;
  mParam = inParam;
  mParam.AddListener(this);
}

MProperty& MProperty::operator=(const MParam& inParam)
{
  mParam = inParam;
  return *this;
}

MProperty& MProperty::operator=(const MInteger& inInt)
{
  mParam = inInt;
  return *this;
}

MProperty& MProperty::operator=(const bool& inBool)
{
  mParam = inBool;
  return *this;
}

MProperty& MProperty::operator=(const MFloat& inFloat)
{
  mParam = inFloat;
  return *this;
}

MProperty& MProperty::operator=(const char* inString)
{
  mParam = inString;
  return *this;
}

MProperty& MProperty::operator=(const MString& inString)
{
  mParam = inString;
  return *this;
}

MProperty& MProperty::operator=(const MVector<MInteger>& inInts)
{
  mParam = inInts;
  return *this;
}

MProperty& MProperty::operator=(const MVector<MFloat>& inFloats)
{
  mParam = inFloats;
  return *this;
}

MProperty& MProperty::operator=(const MVector<MString>& inStrings)
{
  mParam = inStrings;
  return *this;
}

void MProperty::ListenToMessage(const MMessage& inMessage, void* /*ioParam_unused*/)
{
  if (inMessage.GetClass() == kMsg_Modified)
  {
    mModified = true;
    BroadcastMessage(kMsg_Modified, this);
  }
}

