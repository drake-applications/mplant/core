/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MAttachable.h"
#include <algorithm>

// --------------------------------------------------------------
//  Constructor
// --------------------------------------------------------------

MAttachable::MAttachable(void)
{  
}

// --------------------------------------------------------------
//  Destructor
// --------------------------------------------------------------
// Removes all owned attachments.

MAttachable::~MAttachable(void)
{
  if (!mAttachments.empty())
  {
    MAttachment* attachment;
    for (auto iter = mAttachments.begin(); iter != mAttachments.end(); ++iter)
    {
      attachment = *iter;
      if (attachment->Owned())
        delete attachment;  // Only delete if this object
    }                       // owns the attachment.
  }
}

// --------------------------------------------------------------
//  AddAttachment
// --------------------------------------------------------------
// Inserts a new attachment into the list.

void MAttachable::AddAttachment(MAttachment* inAttachment)
{
  mAttachments.push_back(inAttachment);
}

// --------------------------------------------------------------
//  RemoveAttachment
// --------------------------------------------------------------
// Removes an attachment from the list.

bool MAttachable::RemoveAttachment(MAttachment* inAttachment)
{
  bool found = false; // TODO: set if found
  
  if (!mAttachments.empty())
  {
    mAttachments.erase(
      std::remove(mAttachments.begin(), mAttachments.end(), inAttachment),
      mAttachments.end()
    );
  }
  
  return found;
}

// --------------------------------------------------------------
//  ExecuteAttachments
// --------------------------------------------------------------
// Executes all attachments with the given message.

bool MAttachable::ExecuteAttachments(const MMessage& inMsg, void* inData)
{
  bool doAction = false;
  
  if (!mAttachments.empty())
  {
    MAttachment* attachment;
    MUInteger msgClass = inMsg.GetClass();

    for (auto iter = mAttachments.begin(); iter != mAttachments.end(); ++iter)
    {
      attachment = *iter;
      if (attachment->GetMessageClass() & msgClass)
      {
        attachment->Execute(doAction, inMsg, inData);
      }
    }
  }
  return doAction;
}
