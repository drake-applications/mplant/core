/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MArgParser.h"
#include "UString.h"
#include <iostream>
#include <sstream>
#include <algorithm>

// --------------------------------------------------------------
//  Constructors
// --------------------------------------------------------------

MArgParser::MArgParser(void)
{
  Initialize();
}

MArgParser::MArgParser(const MString& inAppName)
{
  mApplication = inAppName;
  Initialize();
}

MArgParser::MArgParser(const MString& inAppName, const MString& inCommand)
{
  mApplication = inAppName;
  mCommand = inCommand;
  Initialize();
}

MArgParser::MArgParser(const MString& inAppName, const MString& inCommand, const MString& inDescription)
{
  mApplication = inAppName;
  mCommand = inCommand;
  mDescription = inDescription;
  Initialize();
}

// --------------------------------------------------------------
//  Initialize
// --------------------------------------------------------------
// Common initialization used by all constructors.

void MArgParser::Initialize(void)
{
  mHasFinal = false;
  mPromptEnabled = true;
  SetArgumentPrompter();
}

// --------------------------------------------------------------
//  Destructor
// --------------------------------------------------------------
// 

MArgParser::~MArgParser(void)
{
}

// --------------------------------------------------------------
//  SetArgumentPrompter
// --------------------------------------------------------------
// Set a custom argument prompter.  The default uses stdin.

void MArgParser::SetArgumentPrompter(MArgPrompter* inPrompter)
{
  if (!inPrompter && !mPrompter)
  {
    inPrompter = new MArgPrompter();
  }

  if (inPrompter)
  {
    mPrompter = inPrompter;
  }
}

void MArgParser::SetArgumentPrompter(const TReference<MArgPrompter>& inPrompter)
{
  if (inPrompter && !mPrompter)
  {
    mPrompter = inPrompter;
  }
}

// --------------------------------------------------------------
//  AddPositional
// --------------------------------------------------------------
// Add a positional argument to the expected argument list

MArgument& MArgParser::AddPositional(const MString& inName, const MSize& inNargs)
{
  if (inNargs == 0)
  {
    std::ostringstream errStream;
    errStream << "Positional argument named '" << inName << "' may not have nargs = 0";
    throw MEXCEPTION(errStream.str());
  }

  MArgument argument(inName, inNargs);
  AddArgument(argument);
  mPositionals.push_back(mArguments.size() - 1);
  return mArguments.back();
}

// --------------------------------------------------------------
//  AddFinal
// --------------------------------------------------------------
// Add a final, named argument.

MArgument& MArgParser::AddFinal(const MString& inName)
{
  if (mHasFinal)
  {
    std::ostringstream errStream;
    errStream << "Attempted to add a second final argument named '" << inName << "'.";
    throw MEXCEPTION(errStream.str());
  }

  mHasFinal = true;
  MArgument argument(inName, MNargs::Plus);
  AddArgument(argument);
  mPositionals.push_back(mArguments.size() - 1);
  return mArguments.back();
}

// --------------------------------------------------------------
//  AddArgument
// --------------------------------------------------------------
// Add a named argument that expects an integral number of
// positional values.

MArgument& MArgParser::AddArgument(const MString& inShort, const MString& inLong, const MSize& inNargs, bool inRequired)
{
  MArgument argument(inShort, inLong, inNargs, inRequired);
  return AddArgument(argument);
}

// --------------------------------------------------------------
//  AddArgument
// --------------------------------------------------------------
// Add a named argument that uses MNargs to indicate a variable
// number of positional values.

MArgument& MArgParser::AddArgument(const MString& inShort, const MString& inLong, const MNargs& inNargs, bool inRequired)
{
  MArgument argument(inShort, inLong, inNargs, inRequired);
  return AddArgument(argument);
}

// --------------------------------------------------------------
//  AddArgument
// --------------------------------------------------------------
// Add a named argument without abbreviation that uses an
// integral number of positional values.

MArgument& MArgParser::AddArgument(const MString& inLong, const MSize& inNargs, bool inRequired)
{
  MArgument argument(kEmptyString, inLong, inNargs, inRequired);
  return AddArgument(argument);
}

// --------------------------------------------------------------
//  AddArgument
// --------------------------------------------------------------
// Add a named argument without abbreviation that uses MNargs to
// indicate a variable number of positional values.

MArgument& MArgParser::AddArgument(const MString& inLong, const MNargs& inNargs, bool inRequired)
{
  MArgument argument(kEmptyString, inLong, inNargs, inRequired);
  return AddArgument(argument);
}

// --------------------------------------------------------------
//  AddArgument [protected]
// --------------------------------------------------------------
// Add a previously constructed argument.

MArgument& MArgParser::AddArgument(const MArgument& inArgument)
{
  MIndex index = mArguments.size();

  mArguments.push_back(inArgument);
  if (inArgument.HasLong())
  {
    mLongIndex[inArgument.GetLong()] = index;
  }
  if (inArgument.HasShort())
  {
    mShortIndex[inArgument.GetShort()] = index;
  }
  
  mFullIndex[inArgument.GetArgumentName()] = index;

  return mArguments.back();
}

// --------------------------------------------------------------
//  AddDeferrer [protected]
// --------------------------------------------------------------
// Indicate that the remaining arguments are deferred until later
// (not checked immediately).  This also defers the --help flag
// if it appears on or after a deferred argument.
//
// NOTE: To retrieve deferred arguments later, use:
//       MArgParser::GetParameter("deferred_args")

MArgument& MArgParser::AddDeferrer()
{
  MArgument argument("deferred_args", MNargs::Star);
  AddArgument(argument);
  mPositionals.push_back(mArguments.size() - 1);
  return mArguments.back();
}


// --------------------------------------------------------------
//  Parse
// --------------------------------------------------------------
// Parse a standard argument list.

void MArgParser::Parse(int argc, const char** argv)
{
  try
  {
    bool explicitFinal = false;
    bool deferred = false;

    MIndex pos = 0;
    MSize numPositionals = mPositionals.size();

    if (mCommand.empty() && argc > 0)
    {
      mCommand = argv[0];
    }

    MIndex index = 0;
    for (auto iter = mArguments.begin(); iter != mArguments.end(); ++iter, ++index)
    {
      mUnusedArguments.push_back(index);
    }

    int i = 1; // Ignore the 1st param

    while (i < argc)
    {
      MString arg = argv[i];
      MString name;
      MIndex argIndex = 0;
      MArgument argument;

      if (arg == "--help" && !deferred)
      {
        // Print usage and get out
        Usage();
        exit(0);
      }
      else if (arg == "--")
      {
        // Read remaining into final argument
        explicitFinal = true;
        if (numPositionals == pos + 1)
        {
          argIndex = mPositionals.back();
          ++pos;
        }
        else if (numPositionals < pos + 1) 
        {
          throw std::runtime_error("Could not find a final argument.");
        }
        else
        {
          throw std::runtime_error("Too many positional arguments remain for --");
        }
        ++i;
      }
      else if (UString::StartsWith(arg, "--"))
      {
        // Look up arg by long name
        name = arg.substr(2);
        if (mLongIndex.find(name) == mLongIndex.end())
        {
          if (deferred)
          {
            if (mPositionals.size() == pos + 1)
            {
              argIndex = mPositionals[pos];
              explicitFinal = true;
            }
            else
            {
              throw std::runtime_error("Failed to defer arguments at: --" + name);
            }
          }
          else
          {
            throw std::runtime_error("Unknown argument: --" + name);
          }
        }
        else
        {
          argIndex = mLongIndex[name];
          ++i;
        }
      }
      else if (UString::StartsWith(arg, "-"))
      {
        // Look up arg by short name
        name = arg.substr(1);
        if (mShortIndex.find(name) == mShortIndex.end())
        {
          if (deferred)
          {
            if (mPositionals.size() == pos + 1)
            {
              argIndex = mPositionals[pos];
              explicitFinal = true;
            }
            else
            {
              throw std::runtime_error("Failed to defer arguments at: -" + name);
            }
          }
          else
          {
            throw std::runtime_error("Unknown argument: -" + name);
          }
        }
        else
        {
          argIndex = mShortIndex[name];
          ++i;
        }
      }
      else
      {
        // Read the next positional argument
        if (mPositionals.size() > pos)
        {
          argIndex = mPositionals[pos];
          ++pos;
        }
        else
        {
          throw std::runtime_error("Too many arguments given");
        }
      }

      argument = mArguments[argIndex];
      argument.Read(i, argc, argv, explicitFinal);
      mArguments[argIndex] = argument;

      if (argument.Defers() && !deferred)
      {
        deferred = true;
        AddDeferrer();
      }

      mUsedArguments.push_back(argIndex);
      mUnusedArguments.erase(std::remove(mUnusedArguments.begin(), mUnusedArguments.end(), argIndex), mUnusedArguments.end());
      mNamedIndex[argument.GetArgumentName()] = argIndex;
    }

    for (auto iter = mUnusedArguments.begin(); iter != mUnusedArguments.end(); ++iter)
    {
      MArgument unusedArg = mArguments[*iter];
      if (unusedArg.mRequired && unusedArg.mPrompt.empty())
      {
        throw std::runtime_error("Argument '" + unusedArg.GetArgumentName() + "' is required but not provided.");
      }
    }

    for (auto iter = mUsedArguments.begin(); iter != mUsedArguments.end(); ++iter)
    {
      MArgument usedArg = mArguments[*iter];
      usedArg.Validate();
    }
  }
  catch (std::exception& ex)
  {
    std::cerr << ex.what() << std::endl;
    Usage();
    exit(1);
  }
}

// --------------------------------------------------------------
//  SetParameterValue
// --------------------------------------------------------------
// Set the actual value of the given argument.

void MArgParser::SetParameterValue(const MString& inArgName, const MParam& inParam, bool inValidate)
{
  auto iter = mFullIndex.find(inArgName);

  if (iter != mFullIndex.end())
  {
    MIndex argIndex = iter->second;
    MArgument arg = mArguments[argIndex];
    arg.mParameter = inParam;

    if (inValidate)
    {
      arg.Validate();
    }

    mArguments[argIndex] = arg;

    mUsedArguments.push_back(argIndex);
    mUnusedArguments.erase(std::remove(mUnusedArguments.begin(), mUnusedArguments.end(), argIndex), mUnusedArguments.end());
    mNamedIndex[inArgName] = argIndex;
  }
  else
  {
    throw MEXCEPTION("Undefined argument '" + inArgName + "'");
  }
}

// --------------------------------------------------------------
//  Usage
// --------------------------------------------------------------
// Print usage information for the defined arguments.

void MArgParser::Usage(void)
{
  if (!mApplication.empty())
  {
    std::cerr << mApplication;
  }
  if (!mCommand.empty())
  {
    std::cerr << " " << mCommand;
  }
  if (!mDescription.empty())
  {
    std::cerr << std::endl << std::endl << mDescription << std::endl;
  }

  if (!mArguments.empty())
  {
    std::cerr << std::endl << "  Arguments:";
    for (auto iter = mArguments.begin(); iter != mArguments.end(); ++iter)
    {
      std::cerr << std::endl << "    " << (*iter).Usage();
    }
  }
  std::cerr << std::endl;
}

// --------------------------------------------------------------
//  Exists
// --------------------------------------------------------------
// Return true if a value for the given argument was supplied.

bool MArgParser::Exists(const MString& inName)
{
  return (mNamedIndex.find(inName) != mNamedIndex.end());
}

// --------------------------------------------------------------
//  GetParameter
// --------------------------------------------------------------
// Return the parameter value of the given argument.

MParam MArgParser::GetParameter(const MString& inName)
{
  MParam param;

  auto iter = mNamedIndex.find(inName);
  if (iter == mNamedIndex.end())
  {
    param.Invalidate();
  }
  else
  {
    param = mArguments[iter->second].mParameter;
  }

  return param;
}

// --------------------------------------------------------------
//  GetParameters
// --------------------------------------------------------------
// Return a map of all supplied arguments and their values.

void MArgParser::GetParameters(MParamMap& outParams)
{
  for (auto iter = mUsedArguments.begin(); iter != mUsedArguments.end(); ++iter)
  {
    MArgument arg = mArguments[*iter];
    outParams[arg.GetArgumentName()] = arg.mParameter;
  }
}

// --------------------------------------------------------------
//  PromptUser
// --------------------------------------------------------------
// Ask the user to supply values for required, missing arguments.

void MArgParser::PromptUser(void)
{
  MVector<MIndex> unused = mUnusedArguments;

  for (auto iter = unused.begin(); iter != unused.end(); ++iter)
  {
    bool accepted = false;
    MArgument arg = mArguments[*iter];

    // First try to fill in missing arguments with defaults.

    if (arg.mHasDefault && (arg.mContingent.empty() || Exists(arg.mContingent)))
    {
      SetParameterValue(arg.GetArgumentName(), arg.GetDefault());
    }

    // Only prompt if the argument has a prompt. If the argument is contingent on
    // another argument, only prompt if that argument was supplied.

    else if (!arg.mPrompt.empty() && (arg.mContingent.empty() || Exists(arg.mContingent)))
    {
      MParam inputParam;
      MArgPromptResult result = MArgPromptResult::None;
      bool promptNeeded = true;

      if (!arg.mOptionalPrompt.empty())
      {
        promptNeeded = mPromptEnabled;
      }

      if (promptNeeded)
      {
        // If prompting is disabled on the parser, throw a missing argument exception.

        if (!mPromptEnabled)
        {
          throw "Missing argument: " + arg.GetArgumentName();
        }

        // Use the assigned prompter to get input from the user.

        while (!accepted)
        {
          result = mPrompter->PromptForInput(arg, inputParam);

          if (result == MArgPromptResult::Abort)
          {
            throw "Argument input was aborted by the user.";
          }
          else if (result == MArgPromptResult::Ignore)
          {
            accepted = true;
          }
          else
          {
            try
            {
              SetParameterValue(arg.GetArgumentName(), inputParam);
              accepted = true;
            }
            catch (std::exception& ex)
            {
              mPrompter->ShowMessage(UString::Format("The value '%s' is not valid.", {inputParam.StringValue()}));
            }
          }
        }
      }
    }
  }
}

